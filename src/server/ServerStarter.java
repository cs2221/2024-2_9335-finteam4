package server;

import admin.AdminFrame;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.CosNaming.*;

import java.awt.*;

public class ServerStarter {
    public static void main(String[] args) {
        try {
            String[] orbArgs = {
                    "-ORBInitialHost", "localhost",
                    "-ORBInitialPort", "2809"
            };

            ORB orb = ORB.init(orbArgs, null);

            POA rootPOA = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
            rootPOA.the_POAManager().activate();

            GameServerImpl serverImpl = new GameServerImpl();

            org.omg.CORBA.Object ref = rootPOA.servant_to_reference(serverImpl);

            NamingContextExt ncRef = NamingContextExtHelper.narrow(
                    orb.resolve_initial_references("NameService")
            );


            NameComponent[] path = ncRef.to_name("BoggledGameServer");
            ncRef.rebind(path, ref);


            EventQueue.invokeLater(() -> {
                try {
                    new AdminFrame(serverImpl).setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });


            orb.run();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
