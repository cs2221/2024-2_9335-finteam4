package server;

import BoggleGame.*;
import utils.RandomLetterGenerator;
import utils.WordChecker;

import javax.swing.*;
import javax.xml.crypto.Data;
import java.sql.*;
import java.util.*;


public class GameServerImpl extends GameServerPOA {

    private List<Long> activeSessions = new ArrayList();
    private Map<Long, Users> activeRoom = new HashMap<>();

    @Override
    public int createUser(String username, String password) {
        try {
            String query = "INSERT INTO Users (username, password, win) VALUES (?, ?, ?)";
            PreparedStatement stmt = DatabaseConnection.getMySQLConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, username);
            stmt.setString(2, password);
            stmt.setInt(3, 0);
            stmt.executeUpdate();


            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getInt(1);
            } else {
                throw new SQLException("User creation failed, no ID obtained.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("User creation failed");
        }
    }

    @Override
    public boolean checkIfUserExist(String username) {
        try  {

            Connection connection = DatabaseConnection.getMySQLConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT COUNT(*) FROM Users WHERE username = ?");

            stmt.setString(1, username);

            System.out.println(stmt.toString());

            try (ResultSet resultSet = stmt.executeQuery()) {
                if (resultSet.next()) {
                    int userCount = resultSet.getInt(1);
                    return userCount > 0;
                }
            }
        } catch (SQLException e) {
            System.err.println("SQL Exception during user existence check: " + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException("Database error while checking user existence.", e);
        }
        return false;
    }

    @Override
    public Users loginUser(String username) {
        Users user = null;

        try {

            Connection conn = DatabaseConnection.getMySQLConnection();

            String query = "SELECT userId, username, password FROM users WHERE username = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, username);

            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                int userId = rs.getInt("userId");
                String retrievedUsername = rs.getString("username");
                String password = rs.getString("password");
                user = new Users(userId, retrievedUsername, password, 0);
            }

            rs.close();
            pstmt.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }


    @Override
    public boolean startSession(int userId) {
        long userIdLong = Long.valueOf(userId);
        if (activeSessions.contains(userIdLong)) {
            return false;
        }
        activeSessions.add(userIdLong);
        return true;
    }

    @Override
    public boolean endSession(int userId) {
        long userIdLong = Long.valueOf(userId);
        if (activeSessions.contains(userIdLong)) {
            activeSessions.remove(userIdLong);
            return true;
        }
        return false;
    }
    @Override
    public boolean isSessionActive(int userId) {
        for (long user: activeSessions){
            System.out.println(user);
        }
        long userIdLong = Long.valueOf(userId);
        return activeSessions.contains(userIdLong);
    }

    @Override
    public Room findActiveRoom(Users users) {
        Room activeRoom = null;
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DatabaseConnection.getMySQLConnection();

            String query = "SELECT roomId, status FROM room WHERE status = true";
            pstmt = conn.prepareStatement(query);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                activeRoom = new Room(rs.getInt("roomId"), rs.getBoolean("status"));
            } else {
                activeRoom = createNewRoom(conn);
            }

            String checkquery = "SELECT * FROM lobby WHERE roomId = ? AND userId = ?";
            pstmt = conn.prepareStatement(checkquery);
            pstmt.setLong(1, activeRoom.roomId);
            pstmt.setLong(2, users.userId);

            rs = pstmt.executeQuery();

            if (!rs.next()){
                String insertRoomQuery = "INSERT INTO `lobby`(`roomId`, `userId`) VALUES (?,?)";
                PreparedStatement statement = conn.prepareStatement(insertRoomQuery);

                statement.setLong(1, activeRoom.roomId);
                statement.setLong(2, users.userId);
                statement.executeUpdate();
                statement.close();
            }

            rs.close();
            pstmt.close();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return activeRoom;
    }

    @Override
    public void updateRoomStatus(Room room) {
        try (Connection conn = DatabaseConnection.getMySQLConnection()) {
            String updateRoomQuery = "UPDATE room SET status = ? WHERE roomId = ?";
            PreparedStatement pstmt = conn.prepareStatement(updateRoomQuery);
            pstmt.setBoolean(1, false);
            pstmt.setInt(2, room.roomId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Users[] getUserList(Room room) {
        List<Users> userList = new ArrayList<>();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            conn = DatabaseConnection.getMySQLConnection();

            String getUsersInRoomQuery =
                    "SELECT u.`userId`, u.`username`, u.`password` " +
                            "FROM `lobby` l " +
                            "JOIN `users` u ON l.`userId` = u.`userId` " +
                            "WHERE l.`roomId` = ?";

            pstmt = conn.prepareStatement(getUsersInRoomQuery);
            pstmt.setLong(1, room.roomId);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                Users user = new Users();
                user.userId = rs.getInt("userId");
                user.username = rs.getString("username");
                user.password = rs.getString("password");
                userList.add(user);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return userList.toArray(new Users[userList.size()]);
    }

    @Override
    public UsersScore[] getUserListScore(Room room, int round) {
        try  {
            Connection conn = DatabaseConnection.getMySQLConnection();

            String selectWordsQuery = "SELECT wu.`user_id`, wu.`word`, u.`username` FROM `word_user` wu " +
                    "JOIN `users` u ON wu.`user_id` = u.`userId` " +
                    "WHERE wu.`room_id` = ? AND wu.`round` = ?";
            PreparedStatement pstmt = conn.prepareStatement(selectWordsQuery);
            pstmt.setInt(1, room.roomId);
            pstmt.setInt(2, round);
            ResultSet resultSet = pstmt.executeQuery();

            Map<String, Integer> wordUserMap = new HashMap<>();


            Map<String, Set<Integer>> encounteredWords = new HashMap<>();


            while (resultSet.next()) {
                String word = resultSet.getString("word");
                int userId = resultSet.getInt("user_id");

                if (!encounteredWords.containsKey(word)) {

                    encounteredWords.put(word, new HashSet<>());
                }
                encounteredWords.get(word).add(userId);
            }


            resultSet.beforeFirst();

            Map<Integer, Set<String>> userWordsMap = new HashMap<>();
            Map<Integer, String> userIdUsernameMap = new HashMap<>();
            while (resultSet.next()) {
                int userId = resultSet.getInt("user_id");
                String word = resultSet.getString("word");
                String username = resultSet.getString("username");

                if (encounteredWords.get(word).size() == 1) {
                    userWordsMap.computeIfAbsent(userId, k -> new HashSet<>()).add(word);
                }
                userIdUsernameMap.put(userId, username);
            }

            List<UsersScore> usersScores = new ArrayList<>();
            for (Map.Entry<Integer, Set<String>> entry : userWordsMap.entrySet()) {
                int userId = entry.getKey();
                int score = calculateScore(entry.getValue());
                String username = userIdUsernameMap.get(userId);
                usersScores.add(new UsersScore(userId, score, username));
            }
            conn.close();
            return usersScores.toArray(new UsersScore[0]);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new UsersScore[0];
    }

    private int calculateScore(Set<String> words) {
        int score = 0;
        Set<String> uniqueWords = new HashSet<>(words);
        for (String word : uniqueWords) {
            score += word.length();
        }
        return score;
    }


    @Override
    public String createCharacter(int room_id, int round) {

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {

            preparedStatement = DatabaseConnection.getMySQLConnection().prepareStatement("SELECT * FROM `character` WHERE `room_id` = ? AND `round` = ?");
            preparedStatement.setInt(1, room_id);
            preparedStatement.setInt(2, round);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                System.out.println(resultSet.getString(4));
                return resultSet.getString(4);
            } else {

                String letter = RandomLetterGenerator.generateRandomLetters();

                preparedStatement = DatabaseConnection.getMySQLConnection().prepareStatement("INSERT INTO `character` (`room_id`, `round`, `letter`) VALUES (?, ?, ?)");

                preparedStatement.setInt(1, room_id);
                preparedStatement.setInt(2, round);
                preparedStatement.setString(3, letter);
                preparedStatement.executeUpdate();

                return letter;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return "Error occurred: " + e.getMessage();
        } finally {

            try {
                if (resultSet != null) resultSet.close();
                if (preparedStatement != null) preparedStatement.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
    @Override
    public boolean submitWord(String word, Room room, Users user, int round) {
        if (WordChecker.isWordInFile(word)) {
            try (Connection conn = DatabaseConnection.getMySQLConnection()) {

                if (room == null) {
                    room = createNewRoom(conn);
                }

                String insertWordQuery = "INSERT INTO word_user (user_id, room_id, round, word) VALUES (?, ?, ?, ?)";
                PreparedStatement pstmt = conn.prepareStatement(insertWordQuery);
                pstmt.setInt(1, user.userId);
                pstmt.setInt(2, room.roomId);
                pstmt.setInt(3, round);
                pstmt.setString(4, word);
                pstmt.executeUpdate();

                pstmt.close();
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public Word[] getWord(Room room, int user_id, int round) {
        List<Word> wordsList = new ArrayList<>();

        try (Connection conn = DatabaseConnection.getMySQLConnection()) {
            String selectWordsQuery = "SELECT `user_id`, `word` FROM `word_user` " +
                    "WHERE `room_id` = ? AND `user_id` = ? AND `round` = ?";
            PreparedStatement pstmt = conn.prepareStatement(selectWordsQuery);
            pstmt.setInt(1, room.roomId);
            pstmt.setInt(2, user_id);
            pstmt.setInt(3, round);
            ResultSet resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                int userId = resultSet.getInt("user_id");
                String word = resultSet.getString("word");
                wordsList.add(new Word(userId, word));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Word[] wordsArray = new Word[wordsList.size()];
        wordsArray = wordsList.toArray(wordsArray);
        return wordsArray;
    }

    @Override
    public void createResult(int user_id, Room room, int round, boolean isWinner, int point) {
        try (Connection conn = DatabaseConnection.getMySQLConnection()) {
            String checkExistenceQuery = "SELECT COUNT(*) FROM result WHERE user_id = ? AND room = ? AND round = ?";
            PreparedStatement checkStmt = conn.prepareStatement(checkExistenceQuery);
            checkStmt.setInt(1, user_id);
            checkStmt.setInt(2, room.roomId);
            checkStmt.setInt(3, round);

            ResultSet resultSet = checkStmt.executeQuery();
            resultSet.next();
            int count = resultSet.getInt(1);

            if (count == 0) {
                String insertResultQuery = "INSERT INTO result (user_id, room, round, isWinner, point) VALUES (?, ?, ?, ?, ?)";
                PreparedStatement insertStmt = conn.prepareStatement(insertResultQuery);
                insertStmt.setInt(1, user_id);
                insertStmt.setInt(2, room.roomId);
                insertStmt.setInt(3, round);
                insertStmt.setBoolean(4, isWinner);
                insertStmt.setInt(5, point);
                insertStmt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public UserResult[] getUserResult(Room room) {
        List<UserResult> resultList = new ArrayList<>();

        try (Connection conn = DatabaseConnection.getMySQLConnection()) {
            String selectResultQuery = "SELECT u.userId, u.username, COALESCE(SUM(r.point), 0) AS totalPoints, " +
                    "COALESCE(SUM(CASE WHEN r.isWinner = true THEN 1 ELSE 0 END), 0) AS totalWin " +
                    "FROM users u " +
                    "JOIN result r ON u.userId = r.user_id " +
                    "WHERE r.room = ? " +
                    "GROUP BY u.userId, u.username " +
                    "ORDER BY totalPoints DESC";
            PreparedStatement pstmt = conn.prepareStatement(selectResultQuery);
            pstmt.setInt(1, room.roomId);
            ResultSet resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                int userId = resultSet.getInt("userId");
                String username = resultSet.getString("username");
                int totalPoints = resultSet.getInt("totalPoints");
                int totalWin = resultSet.getInt("totalWin");

                resultList.add(new UserResult(userId, totalPoints, totalWin, username));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultList.toArray(new UserResult[0]);
    }


    @Override
    public Users[] getUserListLeaderboard() {
        List<Users> userList = new ArrayList<>();

        try (Connection conn = DatabaseConnection.getMySQLConnection()) {
            String selectQuery = "SELECT u.userId, u.username, u.password, r.max_points " +
                    "FROM users u " +
                    "JOIN ( " +
                    "    SELECT user_id, MAX(point) AS max_points " +
                    "    FROM result " +
                    "    GROUP BY user_id " +
                    ") r ON u.userId = r.user_id " +
                    "ORDER BY r.max_points DESC;";


            PreparedStatement pstmt = conn.prepareStatement(selectQuery);
            ResultSet resultSet = pstmt.executeQuery();

            while (resultSet.next()) {
                int userId = resultSet.getInt("userId");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");
                int win = resultSet.getInt("max_points");

                userList.add(new Users(userId, username, password, win));
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }

        return userList.toArray(new Users[0]);
    }


    @Override
    public void updateWinner(int userId) {
        try (Connection conn = DatabaseConnection.getMySQLConnection()) {

            String updateQuery = "UPDATE users SET win = win + 1 WHERE userId = ?";
            PreparedStatement pstmt = conn.prepareStatement(updateQuery);
            pstmt.setInt(1, userId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    private Map<Integer, Integer> roomTimer = new HashMap<>();
    private Map<Integer, Integer> roomUserTimer = new HashMap<>();

    @Override
    public void createTimer(Room room, int user_id) {
        roomTimer.put(room.roomId, getTimerFromDatabase(0));
        roomUserTimer.put(user_id, room.roomId);
    }


    @Override
    public int waitingTime(int roomId) {
        return roomTimer.getOrDefault(roomId, 0);
    }

    @Override
    public boolean isTimerExist(int roomId, int user_id) {

        return roomTimer.containsKey(roomId) || roomUserTimer.containsKey(user_id);
    }


    @Override
    public void updateTime(int roomId, int time, int user_id) {
        if (roomTimer.containsKey(roomId) && roomUserTimer.containsKey(user_id)) {
            int x = roomTimer.get(roomId).intValue() - 1;
            roomTimer.put(roomId, x);
        }
    }

    @Override
    public void removeTime(int roomId) {
        roomTimer.remove(roomId);
    }

    private Map<Integer, Integer> roomTimerGame = new HashMap<>();
    private Map<Integer, Integer> roomUserTimerGame = new HashMap<>();

    @Override
    public void createTimerGame(Room room, int user_id) {
        roomTimerGame.put(room.roomId, getTimerFromDatabase(1));
        roomUserTimerGame.put(room.roomId, user_id);
        System.out.println(user_id + "user");
    }

    @Override
    public int waitingTimeGame(int roomId) {
        return  roomTimerGame.getOrDefault(roomId, 0);
    }

    @Override
    public boolean isTimerExistGame(int roomId, int user_id) {
        System.out.println(roomId + " " + user_id);
        return roomTimerGame.containsKey(roomId) || roomUserTimerGame.containsKey(roomId);
    }

    @Override
    public void updateTimeGame(int roomId, int time, int user_id) {
        System.out.println(user_id + " " + roomId);
        if (roomTimerGame.containsKey(roomId) && roomUserTimerGame.get(roomId) == user_id) {
            int x = roomTimerGame.get(roomId).intValue() - 1;
            roomTimerGame.put(roomId, x);
            System.out.println(user_id + " user update " + roomId);
        }

    }

    @Override
    public void removeTimerGame(int roomId) {
        roomTimerGame.remove(roomId);
    }

    @Override
    public boolean editWaitingTime(int time) {
        return editTimer(time, 0);
    }

    @Override
    public boolean editGameTime(int time) {
        return editTimer(time, 1);
    }

    @Override
    public int getTime(int type) {
        int time = 0;

        String query = "SELECT `second` FROM `timer` WHERE `type` = ?";
        try (Connection connection = DatabaseConnection.getMySQLConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setInt(1, type);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    time = resultSet.getInt("second");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return time;
    }

    private boolean editTimer(int time, int type) {
        try(Connection conn = DatabaseConnection.getMySQLConnection()){
            String sql = "SELECT * FROM room WHERE status = 1";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            if(rs.next()){
                return false;
            }
        }catch (SQLException e){
            e.printStackTrace();
            return true;
        }
        try (Connection conn = DatabaseConnection.getMySQLConnection()) {
            String updateTimerQuery = "UPDATE `timer` SET `second` = ? WHERE `type` = ?";
            PreparedStatement pstmt = conn.prepareStatement(updateTimerQuery);
            pstmt.setInt(1, time);
            pstmt.setInt(2, type);
            int rowsAffected = pstmt.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean isPlayerReady(Room room, int round) {
        return false;
    }

    @Override
    public boolean deleteUser(String username) {

        String deleteQuery = "DELETE FROM users WHERE username = ?";

        try (
                Connection conn = DatabaseConnection.getMySQLConnection();
                PreparedStatement pstmt = conn.prepareStatement(deleteQuery)
        ) {
            pstmt.setString(1, username);
            int rowsAffected = pstmt.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {

            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Users[] getAllUserList() {
        List<Users> userList = new ArrayList<>();
        String query = "SELECT `userId`, `username`, `password`, `win` FROM `users`";

        try (Connection conn = DatabaseConnection.getMySQLConnection();
             PreparedStatement pstmt = conn.prepareStatement(query);
             ResultSet rs = pstmt.executeQuery()) {

            while (rs.next()) {
                int userId = rs.getInt("userId");
                String username = rs.getString("username");
                String password = rs.getString("password");
                int win = rs.getInt("win");

                Users user = new Users(userId, username, password, win);
                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userList.toArray(new Users[0]);
    }

    @Override
    public Users[] searchUserList(String username) {
        List<Users> userList = new ArrayList<>();
        String query = "SELECT `userId`, `username`, `password`, `win` FROM `users` WHERE `username` LIKE ?";

        try (Connection conn = DatabaseConnection.getMySQLConnection();
             PreparedStatement pstmt = conn.prepareStatement(query)) {

            pstmt.setString(1, "%" + username + "%");
            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    int userId = rs.getInt("userId");
                    String userName = rs.getString("username");
                    String password = rs.getString("password");
                    int win = rs.getInt("win");

                    Users user = new Users(userId, userName, password, win);
                    userList.add(user);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userList.toArray(new Users[0]);
    }

    public Users getUserById(String user_id){
        Users user = null;

        try(Connection conn = DatabaseConnection.getMySQLConnection()){
            String query = "SELECT * FROM `users` WHERE username = ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1,user_id);
            ResultSet rs = pst.executeQuery();
            if(rs.next()){
                int userId = rs.getInt("userId");
                String userName = rs.getString("username");
                String password = rs.getString("password");
                int win = rs.getInt("win");

                user = new Users(userId, userName, password, win);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return user;
    }
    public boolean updateUser(Users user) {
        String updateQuery = "UPDATE users SET username = ?, password = ? WHERE userId = ?";

        try (Connection conn = DatabaseConnection.getMySQLConnection();
             PreparedStatement pst = conn.prepareStatement(updateQuery)) {

            pst.setString(1, user.username);
            pst.setString(2, user.password);

            pst.setInt(3, user.userId);

            int rowsUpdated = pst.executeUpdate();

            return rowsUpdated > 0;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }


    private int getTimerFromDatabase(int type) {
        int time = 0;
        try (Connection conn = DatabaseConnection.getMySQLConnection()) {
            String selectTimerQuery = "SELECT `second` FROM `timer` WHERE `type` = ?";
            PreparedStatement pstmt = conn.prepareStatement(selectTimerQuery);
            pstmt.setInt(1, type);
            ResultSet resultSet = pstmt.executeQuery();

            if (resultSet.next()) {
                time = resultSet.getInt("second");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return time;
    }


    private Room createNewRoom(Connection conn) throws SQLException {
        String insertRoomQuery = "INSERT INTO room (status) VALUES (true)";
        PreparedStatement pstmt = conn.prepareStatement(insertRoomQuery, PreparedStatement.RETURN_GENERATED_KEYS);
        pstmt.executeUpdate();

        ResultSet generatedKeys = pstmt.getGeneratedKeys();
        Room newRoom = new Room();

        if (generatedKeys.next()) {
            newRoom.roomId = generatedKeys.getInt(1);
            newRoom.status = true;
        }

        generatedKeys.close();
        pstmt.close();

        return newRoom;
    }

}
