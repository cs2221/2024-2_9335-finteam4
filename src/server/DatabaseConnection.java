package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {

    public static Connection getMySQLConnection() {
        String jdbcUrl = "jdbc:mysql://localhost/bogglegamedb";
        String username = "root";
        String password = "";

        try {
            return DriverManager.getConnection(jdbcUrl, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
