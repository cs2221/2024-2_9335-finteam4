package BoggleGame;

public final class GameResultHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.GameResult value = null;

    public GameResultHolder () {
    }

    public GameResultHolder (BoggleGame.GameResult initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.GameResultHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.GameResultHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.GameResultHelper.type ();
    }
}
