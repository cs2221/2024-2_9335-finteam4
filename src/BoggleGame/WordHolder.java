package BoggleGame;

public final class WordHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.Word value = null;

    public WordHolder () {
    }

    public WordHolder (BoggleGame.Word initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.WordHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.WordHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.WordHelper.type ();
    }

}
