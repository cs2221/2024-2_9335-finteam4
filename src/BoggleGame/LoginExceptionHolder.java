package BoggleGame;

public final class LoginExceptionHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.LoginException value = null;

    public LoginExceptionHolder () {
    }

    public LoginExceptionHolder (BoggleGame.LoginException initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.LoginExceptionHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.LoginExceptionHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.LoginExceptionHelper.type ();
    }
}
