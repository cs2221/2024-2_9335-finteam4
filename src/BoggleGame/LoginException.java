package BoggleGame;

public final class LoginException extends org.omg.CORBA.UserException {
    public String message = null;

    public LoginException ()
    {
        super(LoginExceptionHelper.id());
    }

    public LoginException (String _message) {
        super(LoginExceptionHelper.id());
        message = _message;
    }

    public LoginException (String $reason, String _message) {
        super(LoginExceptionHelper.id() + "  " + $reason);
        message = _message;
    }

}
