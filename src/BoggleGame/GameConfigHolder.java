package BoggleGame;

public final class GameConfigHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.GameConfig value = null;

    public GameConfigHolder () {
    }

    public GameConfigHolder (BoggleGame.GameConfig initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.GameConfigHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.GameConfigHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.GameConfigHelper.type ();
    }

}
