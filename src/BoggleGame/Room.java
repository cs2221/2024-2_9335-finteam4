package BoggleGame;

public final class Room implements org.omg.CORBA.portable.IDLEntity {
    public int roomId = (int)0;
    public boolean status = false;

    public Room () {
    }

    public Room (int _roomId, boolean _status) {
        roomId = _roomId;
        status = _status;
    }
}
