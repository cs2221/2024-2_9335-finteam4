package BoggleGame;

abstract public class ScoreSequenceHelper {
  private static String  _id = "IDL:BoggleGame/ScoreSequence:1.0";

  public static void insert (org.omg.CORBA.Any a, BoggleGame.Score[] that) {
    org.omg.CORBA.portable.OutputStream out = a.create_output_stream ();
    a.type (type ());
    write (out, that);
    a.read_value (out.create_input_stream (), type ());
  }

  public static BoggleGame.Score[] extract (org.omg.CORBA.Any a)
  {
    return read (a.create_input_stream ());
  }

  private static org.omg.CORBA.TypeCode __typeCode = null;
  synchronized public static org.omg.CORBA.TypeCode type () {
    if (__typeCode == null) {
      __typeCode = BoggleGame.ScoreHelper.type ();
      __typeCode = org.omg.CORBA.ORB.init ().create_sequence_tc (0, __typeCode);
      __typeCode = org.omg.CORBA.ORB.init ().create_alias_tc (BoggleGame.ScoreSequenceHelper.id (), "ScoreSequence", __typeCode);
    }
    return __typeCode;
  }

  public static String id ()
  {
    return _id;
  }

  public static BoggleGame.Score[] read (org.omg.CORBA.portable.InputStream istream) {
    BoggleGame.Score value[] = null;
    int _len0 = istream.read_long ();
    value = new BoggleGame.Score[_len0];
    for (int _o1 = 0;_o1 < value.length; ++_o1)
      value[_o1] = BoggleGame.ScoreHelper.read (istream);
    return value;
  }

  public static void write (org.omg.CORBA.portable.OutputStream ostream, BoggleGame.Score[] value) {
    ostream.write_long (value.length);
    for (int _i0 = 0;_i0 < value.length; ++_i0)
      BoggleGame.ScoreHelper.write (ostream, value[_i0]);
  }
}
