package BoggleGame;

public final class Word implements org.omg.CORBA.portable.IDLEntity {
    public int user_id = (int)0;
    public String word = null;

    public Word () {
    }

    public Word (int _user_id, String _word) {
        user_id = _user_id;
        word = _word;
    }

}