package BoggleGame;

abstract public class GameResultHelper {
 private static String  _id = "IDL:BoggleGame/GameResult:1.0";

 public static void insert (org.omg.CORBA.Any a, BoggleGame.GameResult that) {
  org.omg.CORBA.portable.OutputStream out = a.create_output_stream ();
  a.type (type ());
  write (out, that);
  a.read_value (out.create_input_stream (), type ());
 }

 public static BoggleGame.GameResult extract (org.omg.CORBA.Any a)
 {
  return read (a.create_input_stream ());
 }

 private static org.omg.CORBA.TypeCode __typeCode = null;
 private static boolean __active = false;
 synchronized public static org.omg.CORBA.TypeCode type () {
  if (__typeCode == null) {
   synchronized (org.omg.CORBA.TypeCode.class) {
    if (__typeCode == null) {
     if (__active) {
      return org.omg.CORBA.ORB.init().create_recursive_tc ( _id );
     }
     __active = true;
     org.omg.CORBA.StructMember[] _members0 = new org.omg.CORBA.StructMember [2];
     org.omg.CORBA.TypeCode _tcOf_members0 = null;
     _tcOf_members0 = org.omg.CORBA.ORB.init ().create_string_tc (0);
     _members0[0] = new org.omg.CORBA.StructMember (
             "winner",
             _tcOf_members0,
             null);
     _tcOf_members0 = org.omg.CORBA.ORB.init ().create_string_tc (0);
     _members0[1] = new org.omg.CORBA.StructMember (
             "message",
             _tcOf_members0,
             null);
     __typeCode = org.omg.CORBA.ORB.init ().create_struct_tc (BoggleGame.GameResultHelper.id (), "GameResult", _members0);
     __active = false;
    }
   }
  }
  return __typeCode;
 }

 public static String id ()
 {
  return _id;
 }

 public static BoggleGame.GameResult read (org.omg.CORBA.portable.InputStream istream) {
  BoggleGame.GameResult value = new BoggleGame.GameResult ();
  value.winner = istream.read_string ();
  value.message = istream.read_string ();
  return value;
 }

 public static void write (org.omg.CORBA.portable.OutputStream ostream, BoggleGame.GameResult value) {
  ostream.write_string (value.winner);
  ostream.write_string (value.message);
 }
}
