package BoggleGame;

public class _GameServerStub extends org.omg.CORBA.portable.ObjectImpl implements BoggleGame.GameServer {

    public int createUser (String username, String password) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("createUser", true);
            $out.write_string (username);
            $out.write_string (password);
            $in = _invoke ($out);
            int $result = $in.read_long ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return createUser (username, password        );
        } finally {
            _releaseReply ($in);
        }
    }

    public boolean checkIfUserExist (String username) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("checkIfUserExist", true);
            $out.write_string (username);
            $in = _invoke ($out);
            boolean $result = $in.read_boolean ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return checkIfUserExist (username        );
        } finally {
            _releaseReply ($in);
        }
    }

    public BoggleGame.Users loginUser (String username) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("loginUser", true);
            $out.write_string (username);
            $in = _invoke ($out);
            BoggleGame.Users $result = BoggleGame.UsersHelper.read ($in);
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return loginUser (username        );
        } finally {
            _releaseReply ($in);
        }
    }

    public boolean startSession (int userId) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("startSession", true);
            $out.write_long (userId);
            $in = _invoke ($out);
            boolean $result = $in.read_boolean ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return startSession (userId        );
        } finally {
            _releaseReply ($in);
        }
    }

    public boolean endSession (int userId) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("endSession", true);
            $out.write_long (userId);
            $in = _invoke ($out);
            boolean $result = $in.read_boolean ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return endSession (userId        );
        } finally {
            _releaseReply ($in);
        }
    }

    public boolean isSessionActive (int userId) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("isSessionActive", true);
            $out.write_long (userId);
            $in = _invoke ($out);
            boolean $result = $in.read_boolean ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return isSessionActive (userId        );
        } finally {
            _releaseReply ($in);
        }
    }

    public BoggleGame.Room findActiveRoom (BoggleGame.Users users) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("findActiveRoom", true);
            BoggleGame.UsersHelper.write ($out, users);
            $in = _invoke ($out);
            BoggleGame.Room $result = BoggleGame.RoomHelper.read ($in);
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return findActiveRoom (users        );
        } finally {
            _releaseReply ($in);
        }
    }

    public void updateRoomStatus (BoggleGame.Room room) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("updateRoomStatus", true);
            BoggleGame.RoomHelper.write ($out, room);
            $in = _invoke ($out);
            return;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            updateRoomStatus (room        );
        } finally {
            _releaseReply ($in);
        }
    }

    public BoggleGame.Users[] getUserList (BoggleGame.Room room) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("getUserList", true);
            BoggleGame.RoomHelper.write ($out, room);
            $in = _invoke ($out);
            BoggleGame.Users $result[] = BoggleGame.UserInRoomHelper.read ($in);
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return getUserList (room        );
        } finally {
            _releaseReply ($in);
        }
    }

    public BoggleGame.UsersScore[] getUserListScore (BoggleGame.Room room, int round) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("getUserListScore", true);
            BoggleGame.RoomHelper.write ($out, room);
            $out.write_long (round);
            $in = _invoke ($out);
            BoggleGame.UsersScore $result[] = BoggleGame.UserInRoomScoreHelper.read ($in);
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return getUserListScore (room, round        );
        } finally {
            _releaseReply ($in);
        }
    }

    public String createCharacter (int room_id, int round) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("createCharacter", true);
            $out.write_long (room_id);
            $out.write_long (round);
            $in = _invoke ($out);
            String $result = $in.read_string ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return createCharacter (room_id, round        );
        } finally {
            _releaseReply ($in);
        }
    }

    public boolean submitWord (String word, BoggleGame.Room room, BoggleGame.Users user, int round) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("submitWord", true);
            $out.write_string (word);
            BoggleGame.RoomHelper.write ($out, room);
            BoggleGame.UsersHelper.write ($out, user);
            $out.write_long (round);
            $in = _invoke ($out);
            boolean $result = $in.read_boolean ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return submitWord (word, room, user, round        );
        } finally {
            _releaseReply ($in);
        }
    }

    public BoggleGame.Word[] getWord (BoggleGame.Room room, int user_id, int round) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("getWord", true);
            BoggleGame.RoomHelper.write ($out, room);
            $out.write_long (user_id);
            $out.write_long (round);
            $in = _invoke ($out);
            BoggleGame.Word $result[] = BoggleGame.WordSubmittedHelper.read ($in);
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return getWord (room, user_id, round        );
        } finally {
            _releaseReply ($in);
        }
    }

    public void createResult (int user_id, BoggleGame.Room room, int round, boolean isWinner, int point) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("createResult", true);
            $out.write_long (user_id);
            BoggleGame.RoomHelper.write ($out, room);
            $out.write_long (round);
            $out.write_boolean (isWinner);
            $out.write_long (point);
            $in = _invoke ($out);
            return;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            createResult (user_id, room, round, isWinner, point        );
        } finally {
            _releaseReply ($in);
        }
    }

    public BoggleGame.UserResult[] getUserResult (BoggleGame.Room room) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("getUserResult", true);
            BoggleGame.RoomHelper.write ($out, room);
            $in = _invoke ($out);
            BoggleGame.UserResult $result[] = BoggleGame.UserResultsHelper.read ($in);
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return getUserResult (room        );
        } finally {
            _releaseReply ($in);
        }
    }

    public BoggleGame.Users[] getUserListLeaderboard () {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("getUserListLeaderboard", true);
            $in = _invoke ($out);
            BoggleGame.Users $result[] = BoggleGame.LeaderBoardHelper.read ($in);
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return getUserListLeaderboard (        );
        } finally {
            _releaseReply ($in);
        }
    }

    public void updateWinner (int userId) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("updateWinner", true);
            $out.write_long (userId);
            $in = _invoke ($out);
            return;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            updateWinner (userId        );
        } finally {
            _releaseReply ($in);
        }
    }

    public void createTimer (BoggleGame.Room room, int user_id) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("createTimer", true);
            BoggleGame.RoomHelper.write ($out, room);
            $out.write_long (user_id);
            $in = _invoke ($out);
            return;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            createTimer (room, user_id        );
        } finally {
            _releaseReply ($in);
        }
    }

    public int waitingTime (int roomId) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("waitingTime", true);
            $out.write_long (roomId);
            $in = _invoke ($out);
            int $result = $in.read_long ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return waitingTime (roomId        );
        } finally {
            _releaseReply ($in);
        }
    }

    public boolean isTimerExist (int roomId, int user_id) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("isTimerExist", true);
            $out.write_long (roomId);
            $out.write_long (user_id);
            $in = _invoke ($out);
            boolean $result = $in.read_boolean ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return isTimerExist (roomId, user_id        );
        } finally {
            _releaseReply ($in);
        }
    }

    public void updateTime (int roomId, int time, int user_id) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("updateTime", true);
            $out.write_long (roomId);
            $out.write_long (time);
            $out.write_long (user_id);
            $in = _invoke ($out);
            return;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            updateTime (roomId, time, user_id        );
        } finally {
            _releaseReply ($in);
        }
    }

    public void removeTime (int roomId) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("removeTime", true);
            $out.write_long (roomId);
            $in = _invoke ($out);
            return;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            removeTime (roomId        );
        } finally {
            _releaseReply ($in);
        }
    }

    public void createTimerGame (BoggleGame.Room room, int user_id) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("createTimerGame", true);
            BoggleGame.RoomHelper.write ($out, room);
            $out.write_long (user_id);
            $in = _invoke ($out);
            return;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            createTimerGame (room, user_id        );
        } finally {
            _releaseReply ($in);
        }
    }

    public int waitingTimeGame (int roomId) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("waitingTimeGame", true);
            $out.write_long (roomId);
            $in = _invoke ($out);
            int $result = $in.read_long ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return waitingTimeGame (roomId        );
        } finally {
            _releaseReply ($in);
        }
    }

    public boolean isTimerExistGame (int roomId, int user_id)
    {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("isTimerExistGame", true);
            $out.write_long (roomId);
            $out.write_long (user_id);
            $in = _invoke ($out);
            boolean $result = $in.read_boolean ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return isTimerExistGame (roomId, user_id        );
        } finally {
            _releaseReply ($in);
        }
    }

    public void updateTimeGame (int roomId, int time, int user_id)
    {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("updateTimeGame", true);
            $out.write_long (roomId);
            $out.write_long (time);
            $out.write_long (user_id);
            $in = _invoke ($out);
            return;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            updateTimeGame (roomId, time, user_id        );
        } finally {
            _releaseReply ($in);
        }
    }

    public void removeTimerGame (int roomId)
    {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("removeTimerGame", true);
            $out.write_long (roomId);
            $in = _invoke ($out);
            return;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            removeTimerGame (roomId        );
        } finally {
            _releaseReply ($in);
        }
    }

    public boolean editWaitingTime (int time)
    {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("editWaitingTime", true);
            $out.write_long (time);
            $in = _invoke ($out);
            boolean $result = $in.read_boolean ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return editWaitingTime (time        );
        } finally {
            _releaseReply ($in);
        }
    }

    public boolean editGameTime (int time) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("editGameTime", true);
            $out.write_long (time);
            $in = _invoke ($out);
            boolean $result = $in.read_boolean ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return editGameTime (time        );
        } finally {
            _releaseReply ($in);
        }
    }

    public int getTime (int type) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("getTime", true);
            $out.write_long (type);
            $in = _invoke ($out);
            int $result = $in.read_long ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return getTime (type        );
        } finally {
            _releaseReply ($in);
        }
    }

    public boolean isPlayerReady (BoggleGame.Room room, int round) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("isPlayerReady", true);
            BoggleGame.RoomHelper.write ($out, room);
            $out.write_long (round);
            $in = _invoke ($out);
            boolean $result = $in.read_boolean ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return isPlayerReady (room, round        );
        } finally {
            _releaseReply ($in);
        }
    }

    public boolean deleteUser (String username) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("deleteUser", true);
            $out.write_string (username);
            $in = _invoke ($out);
            boolean $result = $in.read_boolean ();
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return deleteUser (username        );
        } finally {
            _releaseReply ($in);
        }
    }

    public BoggleGame.Users[] getAllUserList () {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("getAllUserList", true);
            $in = _invoke ($out);
            BoggleGame.Users $result[] = BoggleGame.GetUserListHelper.read ($in);
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return getAllUserList (        );
        } finally {
            _releaseReply ($in);
        }
    }

    public BoggleGame.Users[] searchUserList (String username) {
        org.omg.CORBA.portable.InputStream $in = null;
        try {
            org.omg.CORBA.portable.OutputStream $out = _request ("searchUserList", true);
            $out.write_string (username);
            $in = _invoke ($out);
            BoggleGame.Users $result[] = BoggleGame.GetUserListHelper.read ($in);
            return $result;
        } catch (org.omg.CORBA.portable.ApplicationException $ex) {
            $in = $ex.getInputStream ();
            String _id = $ex.getId ();
            throw new org.omg.CORBA.MARSHAL (_id);
        } catch (org.omg.CORBA.portable.RemarshalException $rm) {
            return searchUserList (username        );
        } finally {
            _releaseReply ($in);
        }
    }

    private static String[] __ids = {
            "IDL:BoggleGame/GameServer:1.0"};

    public String[] _ids () {
        return (String[])__ids.clone ();
    }

    private void readObject (java.io.ObjectInputStream s) throws java.io.IOException {
        String str = s.readUTF ();
        String[] args = null;
        java.util.Properties props = null;
        org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init (args, props);
        try {
            org.omg.CORBA.Object obj = orb.string_to_object (str);
            org.omg.CORBA.portable.Delegate delegate = ((org.omg.CORBA.portable.ObjectImpl) obj)._get_delegate ();
            _set_delegate (delegate);
        } finally {
            orb.destroy() ;
        }
    }

    private void writeObject (java.io.ObjectOutputStream s) throws java.io.IOException {
        String[] args = null;
        java.util.Properties props = null;
        org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init (args, props);
        try {
            String str = orb.object_to_string (this);
            s.writeUTF (str);
        } finally {
            orb.destroy() ;
        }
    }
}
