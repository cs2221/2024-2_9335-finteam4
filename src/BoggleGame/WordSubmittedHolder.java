package BoggleGame;

public final class WordSubmittedHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.Word value[] = null;

    public WordSubmittedHolder () {
    }

    public WordSubmittedHolder (BoggleGame.Word[] initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.WordSubmittedHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.WordSubmittedHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.WordSubmittedHelper.type ();
    }
}
