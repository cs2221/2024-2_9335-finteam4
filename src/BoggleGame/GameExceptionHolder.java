package BoggleGame;

public final class GameExceptionHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.GameException value = null;

    public GameExceptionHolder () {
    }

    public GameExceptionHolder (BoggleGame.GameException initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.GameExceptionHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.GameExceptionHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.GameExceptionHelper.type ();
    }

}
