package BoggleGame;

public final class Credentials implements org.omg.CORBA.portable.IDLEntity {
    public String username = null;
    public String password = null;

    public Credentials () {
    }

    public Credentials (String _username, String _password) {
        username = _username;
        password = _password;
    }

}
