package BoggleGame;

public final class RoomHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.Room value = null;

    public RoomHolder () {
    }

    public RoomHolder (BoggleGame.Room initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.RoomHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.RoomHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.RoomHelper.type ();
    }

}
