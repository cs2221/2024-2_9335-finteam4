package BoggleGame;

public final class CharMatrixHolder implements org.omg.CORBA.portable.Streamable {
    public char value[][] = null;

    public CharMatrixHolder () {
    }

    public CharMatrixHolder (char[][] initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.CharMatrixHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.CharMatrixHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.CharMatrixHelper.type ();
    }
}
