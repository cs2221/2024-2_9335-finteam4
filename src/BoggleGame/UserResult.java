package BoggleGame;

public final class UserResult implements org.omg.CORBA.portable.IDLEntity {
    public int user_id = (int)0;
    public int score = (int)0;
    public int total_win = (int)0;
    public String username = null;

    public UserResult () {
    }

    public UserResult (int _user_id, int _score, int _total_win, String _username) {
        user_id = _user_id;
        score = _score;
        total_win = _total_win;
        username = _username;
    }

}
