package BoggleGame;

public final class ScoreSequenceHolder implements org.omg.CORBA.portable.Streamable {
  public BoggleGame.Score value[] = null;

  public ScoreSequenceHolder () {
  }

  public ScoreSequenceHolder (BoggleGame.Score[] initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = BoggleGame.ScoreSequenceHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    BoggleGame.ScoreSequenceHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return BoggleGame.ScoreSequenceHelper.type ();
  }
}
