package BoggleGame;

public final class UserInRoomScoreHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.UsersScore value[] = null;

    public UserInRoomScoreHolder () {
    }

    public UserInRoomScoreHolder (BoggleGame.UsersScore[] initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.UserInRoomScoreHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.UserInRoomScoreHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.UserInRoomScoreHelper.type ();
    }
}
