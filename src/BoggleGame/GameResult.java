package BoggleGame;

public final class GameResult implements org.omg.CORBA.portable.IDLEntity {
    public String winner = null;
    public String message = null;

    public GameResult () {
    }

    public GameResult (String _winner, String _message) {
        winner = _winner;
        message = _message;
    }

}
