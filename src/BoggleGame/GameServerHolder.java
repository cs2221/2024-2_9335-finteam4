package BoggleGame;

public final class GameServerHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.GameServer value = null;

    public GameServerHolder () {
    }

    public GameServerHolder (BoggleGame.GameServer initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.GameServerHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.GameServerHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.GameServerHelper.type ();
    }
}
