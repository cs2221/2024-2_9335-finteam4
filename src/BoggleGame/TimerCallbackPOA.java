package BoggleGame;

public abstract class TimerCallbackPOA extends org.omg.PortableServer.Servant
        implements BoggleGame.TimerCallbackOperations, org.omg.CORBA.portable.InvokeHandler {

  private static java.util.Hashtable _methods = new java.util.Hashtable ();
  static {
    _methods.put ("notify", new java.lang.Integer (0));
  }

  public org.omg.CORBA.portable.OutputStream _invoke (String $method, org.omg.CORBA.portable.InputStream in, org.omg.CORBA.portable.ResponseHandler $rh) {
    org.omg.CORBA.portable.OutputStream out = null;
    java.lang.Integer __method = (java.lang.Integer)_methods.get ($method);
    if (__method == null)
      throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);

    switch (__method.intValue ()) {
      case 0:  // BoggleGame/TimerCallback/_notify
      {
        int newWaitingTime = in.read_long ();
        this._notify (newWaitingTime);
        out = $rh.createReply();
        break;
      }

      default:
        throw new org.omg.CORBA.BAD_OPERATION (0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);
    }

    return out;
  }

  private static String[] __ids = {
          "IDL:BoggleGame/TimerCallback:1.0"};

  public String[] _all_interfaces (org.omg.PortableServer.POA poa, byte[] objectId)
  {
    return (String[])__ids.clone ();
  }

  public TimerCallback _this() {
    return TimerCallbackHelper.narrow(
            super._this_object());
  }

  public TimerCallback _this(org.omg.CORBA.ORB orb) {
    return TimerCallbackHelper.narrow(
            super._this_object(orb));
  }


}
