package BoggleGame;

public final class UsersScoreHolder implements org.omg.CORBA.portable.Streamable {
  public BoggleGame.UsersScore value = null;

  public UsersScoreHolder () {
  }

  public UsersScoreHolder (BoggleGame.UsersScore initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = BoggleGame.UsersScoreHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    BoggleGame.UsersScoreHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return BoggleGame.UsersScoreHelper.type ();
  }

}
