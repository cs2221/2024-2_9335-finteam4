package BoggleGame;

public final class UsersScore implements org.omg.CORBA.portable.IDLEntity {
    public int user_id = (int)0;
    public int score = (int)0;
    public String username = null;

    public UsersScore () {
    }

    public UsersScore (int _user_id, int _score, String _username) {
        user_id = _user_id;
        score = _score;
        username = _username;
    }

}
