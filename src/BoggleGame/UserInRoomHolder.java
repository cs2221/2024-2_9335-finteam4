package BoggleGame;

public final class UserInRoomHolder implements org.omg.CORBA.portable.Streamable {
  public BoggleGame.Users value[] = null;

  public UserInRoomHolder () {
  }

  public UserInRoomHolder (BoggleGame.Users[] initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = BoggleGame.UserInRoomHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    BoggleGame.UserInRoomHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return BoggleGame.UserInRoomHelper.type ();
  }
}
