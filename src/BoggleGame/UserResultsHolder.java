package BoggleGame;

public final class UserResultsHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.UserResult value[] = null;

    public UserResultsHolder () {
    }

    public UserResultsHolder (BoggleGame.UserResult[] initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.UserResultsHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.UserResultsHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.UserResultsHelper.type ();
    }

}
