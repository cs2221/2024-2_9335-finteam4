package BoggleGame;

public final class LeaderBoardHolder implements org.omg.CORBA.portable.Streamable {
  public BoggleGame.Users value[] = null;

  public LeaderBoardHolder () {
  }

  public LeaderBoardHolder (BoggleGame.Users[] initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = BoggleGame.LeaderBoardHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    BoggleGame.LeaderBoardHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return BoggleGame.LeaderBoardHelper.type ();
  }
}
