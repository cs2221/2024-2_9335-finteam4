package BoggleGame;

public final class CharSequenceHolder implements org.omg.CORBA.portable.Streamable {
    public char value[] = null;

    public CharSequenceHolder () {
    }

    public CharSequenceHolder (char[] initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.CharSequenceHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.CharSequenceHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.CharSequenceHelper.type ();
    }
}
