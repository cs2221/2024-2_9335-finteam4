package BoggleGame;

public final class GameConfig implements org.omg.CORBA.portable.IDLEntity {
    public int waitingTime = (int)0;
    public int roundDuration = (int)0;

    public GameConfig () {
    }

    public GameConfig (int _waitingTime, int _roundDuration) {
        waitingTime = _waitingTime;
        roundDuration = _roundDuration;
    }
}
