package BoggleGame;

public abstract class GameServerPOA extends org.omg.PortableServer.Servant
        implements BoggleGame.GameServerOperations, org.omg.CORBA.portable.InvokeHandler {

    private static java.util.Hashtable _methods = new java.util.Hashtable();

    static {
        _methods.put("createUser", new java.lang.Integer(0));
        _methods.put("checkIfUserExist", new java.lang.Integer(1));
        _methods.put("loginUser", new java.lang.Integer(2));
        _methods.put("startSession", new java.lang.Integer(3));
        _methods.put("endSession", new java.lang.Integer(4));
        _methods.put("isSessionActive", new java.lang.Integer(5));
        _methods.put("findActiveRoom", new java.lang.Integer(6));
        _methods.put("updateRoomStatus", new java.lang.Integer(7));
        _methods.put("getUserList", new java.lang.Integer(8));
        _methods.put("getUserListScore", new java.lang.Integer(9));
        _methods.put("createCharacter", new java.lang.Integer(10));
        _methods.put("submitWord", new java.lang.Integer(11));
        _methods.put("getWord", new java.lang.Integer(12));
        _methods.put("createResult", new java.lang.Integer(13));
        _methods.put("getUserResult", new java.lang.Integer(14));
        _methods.put("getUserListLeaderboard", new java.lang.Integer(15));
        _methods.put("updateWinner", new java.lang.Integer(16));
        _methods.put("createTimer", new java.lang.Integer(17));
        _methods.put("waitingTime", new java.lang.Integer(18));
        _methods.put("isTimerExist", new java.lang.Integer(19));
        _methods.put("updateTime", new java.lang.Integer(20));
        _methods.put("removeTime", new java.lang.Integer(21));
        _methods.put("createTimerGame", new java.lang.Integer(22));
        _methods.put("waitingTimeGame", new java.lang.Integer(23));
        _methods.put("isTimerExistGame", new java.lang.Integer(24));
        _methods.put("updateTimeGame", new java.lang.Integer(25));
        _methods.put("removeTimerGame", new java.lang.Integer(26));
        _methods.put("editWaitingTime", new java.lang.Integer(27));
        _methods.put("editGameTime", new java.lang.Integer(28));
        _methods.put("getTime", new java.lang.Integer(29));
        _methods.put("isPlayerReady", new java.lang.Integer(30));
        _methods.put("deleteUser", new java.lang.Integer(31));
        _methods.put("getAllUserList", new java.lang.Integer(32));
        _methods.put("searchUserList", new java.lang.Integer(33));
    }

    public org.omg.CORBA.portable.OutputStream _invoke(String $method, org.omg.CORBA.portable.InputStream in, org.omg.CORBA.portable.ResponseHandler $rh) {
        org.omg.CORBA.portable.OutputStream out = null;
        java.lang.Integer __method = (java.lang.Integer) _methods.get($method);
        if (__method == null)
            throw new org.omg.CORBA.BAD_OPERATION(0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);

        switch (__method.intValue()) {
            case 0:  // BoggleGame/GameServer/createUser
            {
                String username = in.read_string();
                String password = in.read_string();
                int $result = (int) 0;
                $result = this.createUser(username, password);
                out = $rh.createReply();
                out.write_long($result);
                break;
            }

            case 1:  // BoggleGame/GameServer/checkIfUserExist
            {
                String username = in.read_string();
                boolean $result = false;
                $result = this.checkIfUserExist(username);
                out = $rh.createReply();
                out.write_boolean($result);
                break;
            }

            case 2:  // BoggleGame/GameServer/loginUser
            {
                String username = in.read_string();
                BoggleGame.Users $result = null;
                $result = this.loginUser(username);
                out = $rh.createReply();
                BoggleGame.UsersHelper.write(out, $result);
                break;
            }

            case 3:  // BoggleGame/GameServer/startSession
            {
                int userId = in.read_long();
                boolean $result = false;
                $result = this.startSession(userId);
                out = $rh.createReply();
                out.write_boolean($result);
                break;
            }

            case 4:  // BoggleGame/GameServer/endSession
            {
                int userId = in.read_long();
                boolean $result = false;
                $result = this.endSession(userId);
                out = $rh.createReply();
                out.write_boolean($result);
                break;
            }

            case 5:  // BoggleGame/GameServer/isSessionActive
            {
                int userId = in.read_long();
                boolean $result = false;
                $result = this.isSessionActive(userId);
                out = $rh.createReply();
                out.write_boolean($result);
                break;
            }

            case 6:  // BoggleGame/GameServer/findActiveRoom
            {
                BoggleGame.Users users = BoggleGame.UsersHelper.read(in);
                BoggleGame.Room $result = null;
                $result = this.findActiveRoom(users);
                out = $rh.createReply();
                BoggleGame.RoomHelper.write(out, $result);
                break;
            }

            case 7:  // BoggleGame/GameServer/updateRoomStatus
            {
                BoggleGame.Room room = BoggleGame.RoomHelper.read(in);
                this.updateRoomStatus(room);
                out = $rh.createReply();
                break;
            }

            case 8:  // BoggleGame/GameServer/getUserList
            {
                BoggleGame.Room room = BoggleGame.RoomHelper.read(in);
                BoggleGame.Users $result[] = null;
                $result = this.getUserList(room);
                out = $rh.createReply();
                BoggleGame.UserInRoomHelper.write(out, $result);
                break;
            }

            case 9:  // BoggleGame/GameServer/getUserListScore
            {
                BoggleGame.Room room = BoggleGame.RoomHelper.read(in);
                int round = in.read_long();
                BoggleGame.UsersScore $result[] = null;
                $result = this.getUserListScore(room, round);
                out = $rh.createReply();
                BoggleGame.UserInRoomScoreHelper.write(out, $result);
                break;
            }

            case 10:  // BoggleGame/GameServer/createCharacter
            {
                int room_id = in.read_long();
                int round = in.read_long();
                String $result = null;
                $result = this.createCharacter(room_id, round);
                out = $rh.createReply();
                out.write_string($result);
                break;
            }

            case 11:  // BoggleGame/GameServer/submitWord
            {
                String word = in.read_string();
                BoggleGame.Room room = BoggleGame.RoomHelper.read(in);
                BoggleGame.Users user = BoggleGame.UsersHelper.read(in);
                int round = in.read_long();
                boolean $result = false;
                $result = this.submitWord(word, room, user, round);
                out = $rh.createReply();
                out.write_boolean($result);
                break;
            }

            case 12:  // BoggleGame/GameServer/getWord
            {
                BoggleGame.Room room = BoggleGame.RoomHelper.read(in);
                int user_id = in.read_long();
                int round = in.read_long();
                BoggleGame.Word $result[] = null;
                $result = this.getWord(room, user_id, round);
                out = $rh.createReply();
                BoggleGame.WordSubmittedHelper.write(out, $result);
                break;
            }

            case 13:  // BoggleGame/GameServer/createResult
            {
                int user_id = in.read_long();
                BoggleGame.Room room = BoggleGame.RoomHelper.read(in);
                int round = in.read_long();
                boolean isWinner = in.read_boolean();
                int point = in.read_long();
                this.createResult(user_id, room, round, isWinner, point);
                out = $rh.createReply();
                break;
            }

            case 14:  // BoggleGame/GameServer/getUserResult
            {
                BoggleGame.Room room = BoggleGame.RoomHelper.read(in);
                BoggleGame.UserResult $result[] = null;
                $result = this.getUserResult(room);
                out = $rh.createReply();
                BoggleGame.UserResultsHelper.write(out, $result);
                break;
            }

            case 15:  // BoggleGame/GameServer/getUserListLeaderboard
            {
                BoggleGame.Users $result[] = null;
                $result = this.getUserListLeaderboard();
                out = $rh.createReply();
                BoggleGame.LeaderBoardHelper.write(out, $result);
                break;
            }

            case 16:  // BoggleGame/GameServer/updateWinner
            {
                int userId = in.read_long();
                this.updateWinner(userId);
                out = $rh.createReply();
                break;
            }

            case 17:  // BoggleGame/GameServer/createTimer
            {
                BoggleGame.Room room = BoggleGame.RoomHelper.read(in);
                int user_id = in.read_long();
                this.createTimer(room, user_id);
                out = $rh.createReply();
                break;
            }

            case 18:  // BoggleGame/GameServer/waitingTime
            {
                int roomId = in.read_long();
                int $result = (int) 0;
                $result = this.waitingTime(roomId);
                out = $rh.createReply();
                out.write_long($result);
                break;
            }

            case 19:  // BoggleGame/GameServer/isTimerExist
            {
                int roomId = in.read_long();
                int user_id = in.read_long();
                boolean $result = false;
                $result = this.isTimerExist(roomId, user_id);
                out = $rh.createReply();
                out.write_boolean($result);
                break;
            }

            case 20:  // BoggleGame/GameServer/updateTime
            {
                int roomId = in.read_long();
                int time = in.read_long();
                int user_id = in.read_long();
                this.updateTime(roomId, time, user_id);
                out = $rh.createReply();
                break;
            }

            case 21:  // BoggleGame/GameServer/removeTime
            {
                int roomId = in.read_long();
                this.removeTime(roomId);
                out = $rh.createReply();
                break;
            }

            case 22:  // BoggleGame/GameServer/createTimerGame
            {
                BoggleGame.Room room = BoggleGame.RoomHelper.read(in);
                int user_id = in.read_long();
                this.createTimerGame(room, user_id);
                out = $rh.createReply();
                break;
            }

            case 23:  // BoggleGame/GameServer/waitingTimeGame
            {
                int roomId = in.read_long();
                int $result = (int) 0;
                $result = this.waitingTimeGame(roomId);
                out = $rh.createReply();
                out.write_long($result);
                break;
            }

            case 24:  // BoggleGame/GameServer/isTimerExistGame
            {
                int roomId = in.read_long();
                int user_id = in.read_long();
                boolean $result = false;
                $result = this.isTimerExistGame(roomId, user_id);
                out = $rh.createReply();
                out.write_boolean($result);
                break;
            }

            case 25:  // BoggleGame/GameServer/updateTimeGame
            {
                int roomId = in.read_long();
                int time = in.read_long();
                int user_id = in.read_long();
                this.updateTimeGame(roomId, time, user_id);
                out = $rh.createReply();
                break;
            }

            case 26:  // BoggleGame/GameServer/removeTimerGame
            {
                int roomId = in.read_long();
                this.removeTimerGame(roomId);
                out = $rh.createReply();
                break;
            }

            case 27:  // BoggleGame/GameServer/editWaitingTime
            {
                int time = in.read_long();
                boolean $result = false;
                $result = this.editWaitingTime(time);
                out = $rh.createReply();
                out.write_boolean($result);
                break;
            }

            case 28:  // BoggleGame/GameServer/editGameTime
            {
                int time = in.read_long();
                boolean $result = false;
                $result = this.editGameTime(time);
                out = $rh.createReply();
                out.write_boolean($result);
                break;
            }

            case 29:  // BoggleGame/GameServer/getTime
            {
                int type = in.read_long();
                int $result = (int) 0;
                $result = this.getTime(type);
                out = $rh.createReply();
                out.write_long($result);
                break;
            }

            case 30:  // BoggleGame/GameServer/isPlayerReady
            {
                BoggleGame.Room room = BoggleGame.RoomHelper.read(in);
                int round = in.read_long();
                boolean $result = false;
                $result = this.isPlayerReady(room, round);
                out = $rh.createReply();
                out.write_boolean($result);
                break;
            }

            case 31:  // BoggleGame/GameServer/deleteUser
            {
                String username = in.read_string();
                boolean $result = false;
                $result = this.deleteUser(username);
                out = $rh.createReply();
                out.write_boolean($result);
                break;
            }

            case 32:  // BoggleGame/GameServer/getAllUserList
            {
                BoggleGame.Users $result[] = null;
                $result = this.getAllUserList();
                out = $rh.createReply();
                BoggleGame.GetUserListHelper.write(out, $result);
                break;
            }

            case 33:  // BoggleGame/GameServer/searchUserList
            {
                String username = in.read_string();
                BoggleGame.Users $result[] = null;
                $result = this.searchUserList(username);
                out = $rh.createReply();
                BoggleGame.GetUserListHelper.write(out, $result);
                break;
            }

            default:
                throw new org.omg.CORBA.BAD_OPERATION(0, org.omg.CORBA.CompletionStatus.COMPLETED_MAYBE);
        }

        return out;
    }

    private static String[] __ids = {
            "IDL:BoggleGame/GameServer:1.0"};

    public String[] _all_interfaces(org.omg.PortableServer.POA poa, byte[] objectId) {
        return (String[]) __ids.clone();
    }

    public GameServer _this() {
        return GameServerHelper.narrow(
                super._this_object());
    }

    public GameServer _this(org.omg.CORBA.ORB orb) {
        return GameServerHelper.narrow(
                super._this_object(orb));
    }
}
