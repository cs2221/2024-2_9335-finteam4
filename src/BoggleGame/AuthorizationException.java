package BoggleGame;

public final class AuthorizationException extends org.omg.CORBA.UserException {
    public String message = null;

    public AuthorizationException ()
    {
        super(AuthorizationExceptionHelper.id());
    }

    public AuthorizationException (String _message) {
        super(AuthorizationExceptionHelper.id());
        message = _message;
    }


    public AuthorizationException (String $reason, String _message) {
        super(AuthorizationExceptionHelper.id() + "  " + $reason);
        message = _message;
    }
}
