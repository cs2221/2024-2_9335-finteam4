package BoggleGame;

public final class ScoreHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.Score value = null;

    public ScoreHolder () {
    }

    public ScoreHolder (BoggleGame.Score initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.ScoreHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.ScoreHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.ScoreHelper.type ();
    }
}
