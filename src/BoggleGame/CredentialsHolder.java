package BoggleGame;

public final class CredentialsHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.Credentials value = null;

    public CredentialsHolder () {
    }

    public CredentialsHolder (BoggleGame.Credentials initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.CredentialsHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.CredentialsHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.CredentialsHelper.type ();
    }

}
