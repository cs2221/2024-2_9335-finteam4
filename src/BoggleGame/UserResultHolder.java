package BoggleGame;

public final class UserResultHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.UserResult value = null;

    public UserResultHolder () {
    }

    public UserResultHolder (BoggleGame.UserResult initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.UserResultHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.UserResultHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.UserResultHelper.type ();
    }

}
