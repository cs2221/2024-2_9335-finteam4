package BoggleGame;

public final class UsersHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.Users value = null;

    public UsersHolder () {
    }

    public UsersHolder (BoggleGame.Users initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.UsersHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.UsersHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.UsersHelper.type ();
    }

}
