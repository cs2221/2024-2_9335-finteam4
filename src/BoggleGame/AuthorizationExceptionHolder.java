package BoggleGame;

public final class AuthorizationExceptionHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.AuthorizationException value = null;

    public AuthorizationExceptionHolder () {
    }

    public AuthorizationExceptionHolder (BoggleGame.AuthorizationException initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i) {
        value = BoggleGame.AuthorizationExceptionHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o) {
        BoggleGame.AuthorizationExceptionHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.AuthorizationExceptionHelper.type ();
    }
}
