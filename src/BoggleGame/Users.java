package BoggleGame;

public final class Users implements org.omg.CORBA.portable.IDLEntity {
    public int userId = (int)0;
    public String username = null;
    public String password = null;
    public int win = (int)0;

    public Users () {
    }

    public Users (int _userId, String _username, String _password, int _win) {
        userId = _userId;
        username = _username;
        password = _password;
        win = _win;
    }
}
