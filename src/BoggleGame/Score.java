package BoggleGame;

public final class Score implements org.omg.CORBA.portable.IDLEntity {
    public String username = null;
    public int score = (int)0;

    public Score () {
    }

    public Score (String _username, int _score) {
        username = _username;
        score = _score;
    }

}
