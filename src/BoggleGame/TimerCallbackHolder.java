package BoggleGame;

public final class TimerCallbackHolder implements org.omg.CORBA.portable.Streamable {
  public BoggleGame.TimerCallback value = null;

  public TimerCallbackHolder () {
  }

  public TimerCallbackHolder (BoggleGame.TimerCallback initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = BoggleGame.TimerCallbackHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    BoggleGame.TimerCallbackHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return BoggleGame.TimerCallbackHelper.type ();
  }
}
