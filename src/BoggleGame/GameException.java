package BoggleGame;

public final class GameException extends org.omg.CORBA.UserException {
    public String message = null;

    public GameException ()
    {
        super(GameExceptionHelper.id());
    } // ctor

    public GameException (String _message) {
        super(GameExceptionHelper.id());
        message = _message;
    }


    public GameException (String $reason, String _message) {
        super(GameExceptionHelper.id() + "  " + $reason);
        message = _message;
    }

}
