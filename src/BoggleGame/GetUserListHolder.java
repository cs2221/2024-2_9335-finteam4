package BoggleGame;

public final class GetUserListHolder implements org.omg.CORBA.portable.Streamable {
    public BoggleGame.Users value[] = null;

    public GetUserListHolder () {
    }

    public GetUserListHolder (BoggleGame.Users[] initialValue)
    {
        value = initialValue;
    }

    public void _read (org.omg.CORBA.portable.InputStream i)
    {
        value = BoggleGame.GetUserListHelper.read (i);
    }

    public void _write (org.omg.CORBA.portable.OutputStream o)
    {
        BoggleGame.GetUserListHelper.write (o, value);
    }

    public org.omg.CORBA.TypeCode _type ()
    {
        return BoggleGame.GetUserListHelper.type ();
    }
}
