package BoggleGame;

public interface GameServerOperations
{
  int createUser (String username, String password);
  boolean checkIfUserExist (String username);
  BoggleGame.Users loginUser (String username);
  boolean startSession (int userId);
  boolean endSession (int userId);
  boolean isSessionActive (int userId);
  BoggleGame.Room findActiveRoom (BoggleGame.Users users);
  void updateRoomStatus (BoggleGame.Room room);
  BoggleGame.Users[] getUserList (BoggleGame.Room room);
  BoggleGame.UsersScore[] getUserListScore (BoggleGame.Room room, int round);
  String createCharacter (int room_id, int round);
  boolean submitWord (String word, BoggleGame.Room room, BoggleGame.Users user, int round);
  BoggleGame.Word[] getWord (BoggleGame.Room room, int user_id, int round);
  void createResult (int user_id, BoggleGame.Room room, int round, boolean isWinner, int point);
  BoggleGame.UserResult[] getUserResult (BoggleGame.Room room);
  BoggleGame.Users[] getUserListLeaderboard ();
  void updateWinner (int userId);
  void createTimer (BoggleGame.Room room, int user_id);
  int waitingTime (int roomId);
  boolean isTimerExist (int roomId, int user_id);
  void updateTime (int roomId, int time, int user_id);
  void removeTime (int roomId);
  void createTimerGame (BoggleGame.Room room, int user_id);
  int waitingTimeGame (int roomId);
  boolean isTimerExistGame (int roomId, int user_id);
  void updateTimeGame (int roomId, int time, int user_id);
  void removeTimerGame (int roomId);
  boolean editWaitingTime (int time);
  boolean editGameTime (int time);
  int getTime (int type);
  boolean isPlayerReady (BoggleGame.Room room, int round);
  boolean deleteUser (String username);
  BoggleGame.Users[] getAllUserList ();
  BoggleGame.Users[] searchUserList (String username);
}
