package utils;

import java.util.Random;

public class RandomLetterGenerator {

    public static String generateRandomLetters() {
        Random random = new Random();
        StringBuilder randomLetters = new StringBuilder();
        int vowelsCount = 0;
        int consonantsCount = 0;


        char[] vowels = {'a', 'e', 'i', 'o', 'u'};
        char[] consonants = {'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'};

        while (randomLetters.length() < 20) {

            boolean isVowel = random.nextBoolean();

            if (isVowel && vowelsCount < 7) {

                int randomIndex = random.nextInt(vowels.length);
                randomLetters.append(vowels[randomIndex]);
                vowelsCount++;
            } else if (consonantsCount < 13) {

                int randomIndex = random.nextInt(consonants.length);
                randomLetters.append(consonants[randomIndex]);
                consonantsCount++;
            }
        }

        System.out.println(randomLetters);
        System.out.println(randomLetters.length());

        return randomLetters.toString();
    }

}
