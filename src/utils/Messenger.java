package utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Messenger {

    public static void displayError(String error, JLabel lblError) {
        lblError.setForeground(Color.RED);
        lblError.setText(error);
        Timer timer = new Timer(2000, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                lblError.setText(null);
                lblError.setBorder(null);
            }
        });
        timer.setRepeats(false);
        timer.start();
    }

    public static void displaySuccess(String error, JLabel lblError) {
        lblError.setForeground(Color.GREEN);
        lblError.setText(error);
        Timer timer = new Timer(2000, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                lblError.setText(null);
                lblError.setBorder(null);
            }
        });
        timer.setRepeats(false);
        timer.start();
    }
}
