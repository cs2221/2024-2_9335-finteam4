package utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class WordChecker {
    public static boolean isWordInFile(String word) {
        System.out.println(word);
        try (BufferedReader reader = new BufferedReader(new FileReader(System.getProperty("user.dir")+"\\src\\utils\\words.txt"))) {
            String line;
            while ((line = reader.readLine()) != null) {

                if (line.trim().equalsIgnoreCase(word)) {
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
