package admin;

import BoggleGame.GameServer;
import BoggleGame.Users;
import server.GameServerImpl;
import utils.Messenger;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class UpdatePlayer extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JPasswordField tfPassword;
    private JPasswordField tfRetypePassword;
    private JTextField tfUsername;
    private JLabel lblError;

    public UpdatePlayer(GameServerImpl serverImpl, String user_id) {
        Users users = serverImpl.getUserById(user_id);
        System.out.println(user_id);
        setResizable(false);
        setTitle("Boggled Game");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 640, 619);

        contentPane = new JPanel();

        contentPane.setBackground(new Color(255, 128, 255));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));

        JLabel lblNewLabel = new JLabel("WELCOME TO BOGGLED");
        lblNewLabel.setBackground(Color.CYAN);
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setVerticalAlignment(SwingConstants.CENTER);
        lblNewLabel.setFont(new Font("Arial", Font.BOLD, 30));
        contentPane.add(lblNewLabel, BorderLayout.NORTH);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(null);

        JLabel lblNewLabel_1 = new JLabel("Username");
        lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblNewLabel_1.setBounds(130, 64, 354, 40);
        panel.add(lblNewLabel_1);

        JLabel lblNewLabel_1_1 = new JLabel("Password");
        lblNewLabel_1_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblNewLabel_1_1.setBounds(130, 170, 354, 40);
        panel.add(lblNewLabel_1_1);

        JButton btnRegister = new JButton("Sign Up");
        btnRegister.setFont(new Font("Tahoma", Font.PLAIN, 20));
        btnRegister.setBounds(130, 409, 354, 40);
        panel.add(btnRegister);

        JLabel lblNewLabel_1_1_1 = new JLabel("Re-Enter Password:");
        lblNewLabel_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblNewLabel_1_1_1.setBounds(130, 285, 354, 40);
        panel.add(lblNewLabel_1_1_1);

        lblError = new JLabel("");
        lblError.setForeground(Color.RED);
        lblError.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblError.setBounds(130, 463, 354, 40);
        panel.add(lblError);

        tfPassword = new JPasswordField();
        tfPassword.setText(users.password);
        tfPassword.setBounds(130, 220, 354, 40);
        panel.add(tfPassword);

        tfRetypePassword = new JPasswordField();
        tfRetypePassword.setBounds(130, 335, 354, 40);
        tfRetypePassword.setText(users.password);
        panel.add(tfRetypePassword);

        tfUsername = new JTextField();
        tfUsername.setBounds(130, 114, 363, 40);
        tfUsername.setText(users.username);
        panel.add(tfUsername);
        tfUsername.setColumns(10);


        btnRegister.addActionListener(e -> {
            String username = tfUsername.getText().toString();

            String password = new String(tfPassword.getPassword());  // Correct way for JPasswordField
            String rpassword = new String(tfRetypePassword.getPassword());

            if(username.isEmpty() || password.isEmpty() || rpassword.isEmpty()){
                Messenger.displayError("Please enter the username and password", lblError);
                return;
            }
            if(!password.equals(rpassword)){
                Messenger.displayError("Password does not match",lblError);
                return;
            }
            if(serverImpl.checkIfUserExist(username) || users.username.equals(username)){
                Messenger.displayError("Username is already exist!", lblError);
                return;
            }
            Users updateUser = new Users();
            updateUser.username = username;
            updateUser.password = password;
            updateUser.userId = users.userId;
            serverImpl.updateUser(updateUser);
            Messenger.displaySuccess("Username update successfully!", lblError);

        });
    }



}
