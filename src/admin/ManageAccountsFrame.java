package admin;

import BoggleGame.Users;
import server.GameServerImpl;

import java.awt.Color;
import java.awt.Font;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class ManageAccountsFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField textField;
    private JTable table;

    public ManageAccountsFrame(GameServerImpl serverImpl) {
        setTitle("Manage User");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 805, 609);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(255, 128, 255));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

        contentPane.setLayout(null);

        JPanel panel = new JPanel();
        panel.setBounds(50, 88, 700, 415);
        contentPane.add(panel);
        panel.setLayout(null);

        JButton btnNewButton_1 = new JButton("Search");
        btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnNewButton_1.setBounds(568, 10, 122, 38);
        panel.add(btnNewButton_1);

        btnNewButton_1.addActionListener(e -> {
            String query = textField.getText().trim();
            Users[] users = serverImpl.getAllUserList();
            if (!query.isEmpty()){
                users = serverImpl.searchUserList(query);
            }

            Object[][] data = new Object[users.length][1];
            for (int i = 0; i < users.length; i++) {
                data[i][0] = users[i].username;
            }
            DefaultTableModel model = new DefaultTableModel(data, new String[] { "Player Name" }) {
                boolean[] columnEditables = new boolean[] { false };
                public boolean isCellEditable(int row, int column) {
                    return columnEditables[column];
                }
            };
            table.setModel(model);
        });

        textField = new JTextField();
        textField.setBounds(10, 10, 548, 38);
        panel.add(textField);
        textField.setColumns(10);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 70, 680, 340);
        panel.add(scrollPane);

        table = new JTable();
        table.setFont(new Font("Tahoma", Font.PLAIN, 20));

        Users[] users = serverImpl.getAllUserList();

        if (users != null && users.length > 0) {
            Object[][] data = new Object[users.length][1];
            for (int i = 0; i < users.length; i++) {
                data[i][0] = users[i].username;
            }

            String[] columnNames = { "Player Name" };

            DefaultTableModel model = new DefaultTableModel(data, columnNames) {
                boolean[] columnEditables = new boolean[] { false };
                public boolean isCellEditable(int row, int column) {
                    return columnEditables[column];
                }
            };

            table.setModel(model);
            table.getColumnModel().getColumn(0).setResizable(false);
            table.setRowHeight(30);
        } else {

            JOptionPane.showMessageDialog(null, "No users found.", "Information", JOptionPane.INFORMATION_MESSAGE);
        }

        scrollPane.setViewportView(table);

        JButton btnNewButton = new JButton("Back");
        btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnNewButton.setBounds(10, 11, 122, 38);
        btnNewButton.addActionListener(e -> {
            hide();
        });
        contentPane.add(btnNewButton);

        JButton btnDelete = new JButton("Delete");
        btnDelete.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnDelete.setBounds(309, 514, 180, 38);
        btnDelete.addActionListener(e ->{
            int selectedRow = table.getSelectedRow();
            if (selectedRow != -1) {
                int confirm = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete the selected user?", "Confirm Deletion", JOptionPane.YES_NO_OPTION);
                if (confirm == JOptionPane.YES_OPTION) {
                    DefaultTableModel models = (DefaultTableModel) table.getModel();
                    String selectedUser = (String) models.getValueAt(selectedRow, 0);
                    boolean deleted = serverImpl.deleteUser(selectedUser);
                    if (deleted) {
                        JOptionPane.showMessageDialog(null, "User '" + selectedUser + "' deleted successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);
                        models.removeRow(selectedRow);
                    } else {
                        JOptionPane.showMessageDialog(null, "Error occurred while deleting user.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "No user selected.", "Warning", JOptionPane.WARNING_MESSAGE);
            }
        });
        contentPane.add(btnDelete);

        JButton btnUpdate = new JButton("Update");
        btnUpdate.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnUpdate.setBounds(390, 514, 180, 38);
        btnUpdate.addActionListener(e ->{
            int selectedRow = table.getSelectedRow();
            if (selectedRow != -1) {
                int confirm = JOptionPane.showConfirmDialog(null, "Are you sure you want to update the selected user?", "Confirm Deletion", JOptionPane.YES_NO_OPTION);
                if (confirm == JOptionPane.YES_OPTION) {
                    DefaultTableModel models = (DefaultTableModel) table.getModel();
                    String selectedUser = (String) models.getValueAt(selectedRow, 0);

                    new UpdatePlayer(serverImpl, selectedUser).show();
                }
            } else {
                JOptionPane.showMessageDialog(null, "No user selected.", "Warning", JOptionPane.WARNING_MESSAGE);
            }
        });
        contentPane.add(btnUpdate);

        JLabel lblNewLabel = new JLabel("Manage User");
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 30));
        lblNewLabel.setBounds(152, 3, 523, 54);
        contentPane.add(lblNewLabel);

        setContentPane(contentPane);
    }
}
