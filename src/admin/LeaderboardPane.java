package admin;

import BoggleGame.Users;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.*;

public class LeaderboardPane extends JPanel {

    private static final long serialVersionUID = 1L;

    public LeaderboardPane(int i, Users user) {
        setPreferredSize(new Dimension(740, 54));

        setLayout(null);
        setBounds(0, 0, 740, 54);

        JLabel pos5 = new JLabel(i + " " + user.username);
        pos5.setFont(new Font("Tahoma", Font.PLAIN, 20));
        pos5.setBounds(10, 11, 340, 36);
        add(pos5);

        JLabel pos5Wins = new JLabel("Points: " + user.win);
        pos5Wins.setHorizontalAlignment(SwingConstants.TRAILING);
        pos5Wins.setFont(new Font("Tahoma", Font.PLAIN, 20));
        pos5Wins.setBounds(380, 11, 340, 36);

        add(pos5Wins);
    }
}
