package admin;

import server.GameServerImpl;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class AdminFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;

    public AdminFrame(GameServerImpl serverImpl) {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 646, 589);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(255, 128, 255));
        setLocationRelativeTo(null);
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        JLabel lblNewLabel = new JLabel("WELCOME TO BOGGLED");
        lblNewLabel.setVerticalAlignment(SwingConstants.CENTER);
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setFont(new Font("Arial", Font.BOLD, 30));
        lblNewLabel.setBackground(Color.CYAN);
        lblNewLabel.setBounds(0, 0, 632, 36);
        contentPane.add(lblNewLabel);

        JButton btnNewButton = new JButton("Create Account");
        btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
        btnNewButton.setBounds(125, 148, 395, 48);
        contentPane.add(btnNewButton);

        btnNewButton.addActionListener(e -> {
            new RegisterFrame(serverImpl).setVisible(true);
        });

        JButton btnNewButton_1 = new JButton("Manage Accounts");
        btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
        btnNewButton_1.setBounds(125, 231, 395, 48);
        btnNewButton_1.addActionListener(e ->{
            new ManageAccountsFrame(serverImpl).show();
        });
        contentPane.add(btnNewButton_1);

        JButton btnNewButton_1_1 = new JButton("Leaderboards");
        btnNewButton_1_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
        btnNewButton_1_1.setBounds(125, 308, 395, 48);
        btnNewButton_1_1.addActionListener(e ->{
            new LeaderboardFrame(serverImpl).show();
        });
        contentPane.add(btnNewButton_1_1);

        JButton btnNewButton_1_1_1 = new JButton("Edit Time: Round/Waiting");
        btnNewButton_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
        btnNewButton_1_1_1.setBounds(125, 390, 395, 48);
        btnNewButton_1_1_1.addActionListener(e -> {
            new EditTimerFrame(serverImpl).show();
        });

        contentPane.add(btnNewButton_1_1_1);
    }
}