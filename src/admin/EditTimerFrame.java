package admin;

import server.GameServerImpl;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Font;

public class EditTimerFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField txtRoundTimer;
    private JTextField txtWaitTimer;


    public EditTimerFrame(GameServerImpl serverImpl) {
        setTitle("Edit Timer");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 805, 609);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(255, 128, 255));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

        setContentPane(contentPane);
        contentPane.setLayout(null);

        JPanel panel = new JPanel();
        panel.setBounds(50, 88, 335, 177);
        contentPane.add(panel);
        panel.setLayout(null);

        JLabel lblNewLabel = new JLabel("Round Timer");
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
        lblNewLabel.setBounds(94, 11, 137, 30);
        panel.add(lblNewLabel);

        JLabel lblRoundTimer = new JLabel(serverImpl.getTime(1)+"s");
        lblRoundTimer.setHorizontalAlignment(SwingConstants.CENTER);
        lblRoundTimer.setFont(new Font("Tahoma", Font.PLAIN, 48));
        lblRoundTimer.setBounds(60, 43, 207, 96);
        panel.add(lblRoundTimer);

        JButton btnNewButton = new JButton("Back");
        btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnNewButton.setBounds(10, 11, 122, 38);
        btnNewButton.addActionListener(e ->{
            hide();

        });
        contentPane.add(btnNewButton);

        JPanel panel_1 = new JPanel();
        panel_1.setLayout(null);
        panel_1.setBounds(411, 88, 335, 177);
        contentPane.add(panel_1);

        JLabel lblWaitingTimer = new JLabel("Waiting Timer");
        lblWaitingTimer.setHorizontalAlignment(SwingConstants.CENTER);
        lblWaitingTimer.setFont(new Font("Tahoma", Font.BOLD, 16));
        lblWaitingTimer.setBounds(97, 11, 137, 30);
        panel_1.add(lblWaitingTimer);

        JLabel lblWaitTimer = new JLabel(serverImpl.getTime(0)+"s");
        lblWaitTimer.setHorizontalAlignment(SwingConstants.CENTER);
        lblWaitTimer.setFont(new Font("Tahoma", Font.PLAIN, 48));
        lblWaitTimer.setBounds(63, 43, 207, 96);
        panel_1.add(lblWaitTimer);

        txtRoundTimer = new JTextField();
        txtRoundTimer.setHorizontalAlignment(SwingConstants.CENTER);
        txtRoundTimer.setFont(new Font("Tahoma", Font.PLAIN, 16));
        txtRoundTimer.setBounds(50, 276, 335, 38);
        contentPane.add(txtRoundTimer);
        txtRoundTimer.setColumns(10);

        JButton btnRoundTimer = new JButton("Change");
        btnRoundTimer.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnRoundTimer.setBounds(50, 325, 335, 38);
        btnRoundTimer.addActionListener(e ->{
            try {
                int second = Integer.parseInt(txtRoundTimer.getText());
                boolean result = serverImpl.editGameTime(second);

                if (second < 0) {
                    JOptionPane.showMessageDialog(null, "Waiting time cannot be less than zero.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                if (result) {
                    JOptionPane.showMessageDialog(null, "Round timer updated successfully.");
                    lblRoundTimer.setText(second + "s");
                } else {
                    JOptionPane.showMessageDialog(null, "Failed to update round timer.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (NumberFormatException ex) {

                JOptionPane.showMessageDialog(null, "Please enter a valid number.", "Error", JOptionPane.ERROR_MESSAGE);
            } catch (Exception ex) {

                JOptionPane.showMessageDialog(null, "An error occurred: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }

        });
        contentPane.add(btnRoundTimer);

        txtWaitTimer = new JTextField();
        txtWaitTimer.setHorizontalAlignment(SwingConstants.CENTER);
        txtWaitTimer.setFont(new Font("Tahoma", Font.PLAIN, 16));
        txtWaitTimer.setColumns(10);
        txtWaitTimer.setBounds(411, 276, 335, 38);
        contentPane.add(txtWaitTimer);

        JButton btnWaitTimer = new JButton("Change");
        btnWaitTimer.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnWaitTimer.setBounds(411, 325, 335, 38);
        btnWaitTimer.addActionListener(e ->{
            try {
                int second = Integer.parseInt(txtRoundTimer.getText());


                if (second < 0) {
                    JOptionPane.showMessageDialog(null, "Waiting time cannot be less than zero.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                boolean result = serverImpl.editGameTime(second);

                if (result) {
                    JOptionPane.showMessageDialog(null, "Round timer updated successfully.");
                    lblRoundTimer.setText(second + "s");
                } else {
                    JOptionPane.showMessageDialog(null, "Time can't be updated right now; the player is still playing the game.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (NumberFormatException ex) {

                JOptionPane.showMessageDialog(null, "Please enter a valid number.", "Error", JOptionPane.ERROR_MESSAGE);
            } catch (Exception ex) {

                JOptionPane.showMessageDialog(null, "An error occurred: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }

        });
        contentPane.add(btnWaitTimer);
    }

}