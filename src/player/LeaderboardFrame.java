package player;

import BoggleGame.GameServer;
import BoggleGame.Users;
import server.GameServerImpl;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class LeaderboardFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;

    public LeaderboardFrame(GameServer gameServer) {
        setTitle("Leaderboards");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 806, 604);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(255, 128, 255));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

        setContentPane(contentPane);
        contentPane.setLayout(null);

        JButton btnNewButton = new JButton("Back");
        btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnNewButton.setBounds(10, 11, 119, 41);
        btnNewButton.addActionListener(e -> {
            setVisible(false);
        });
        contentPane.add(btnNewButton);

        JLabel lblNewLabel = new JLabel("Leaderboards");
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 24));
        lblNewLabel.setBounds(287, 9, 184, 41);
        contentPane.add(lblNewLabel);

        JPanel panel = new JPanel();
        panel.setBackground(new Color(255, 0, 255));
        panel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));

        JScrollPane scrollPane = new JScrollPane(panel);
        scrollPane.setBounds(10, 84, 772, 473);
        contentPane.add(scrollPane);

        Users[] users = gameServer.getUserListLeaderboard();


        for (int x = 0; x < users.length; x++) {
            panel.add(new LeaderboardPane(x + 1, users[x]));
        }
        int panelHeight = users.length * 56;
        panel.setPreferredSize(new Dimension(752, panelHeight));
    }
}



