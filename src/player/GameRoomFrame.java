package player;

import BoggleGame.GameServer;
import BoggleGame.Room;
import BoggleGame.Users;
import BoggleGame.UsersScore;

import java.awt.EventQueue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import java.util.Timer;
import java.util.TimerTask;

public class GameRoomFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    DefaultListModel<String> words = new DefaultListModel<>();
    DefaultListModel<String> players = new DefaultListModel<>();
    private static final char[] alphabet = {
            'a','b','c','d','e','f','g',
            'h','i','j','k','l','m','n',
            'o','p','q','r','s','t','u',
            'v','w','x','y','z'
    };
    private final Room room;

    private JPanel contentPane;
    private JTextField userInput;
    private JLabel letter1;
    private JLabel letter2;
    private JLabel letter3;
    private JLabel letter4;
    private JLabel letter5;
    private JLabel letter6;
    private JLabel letter7;
    private JLabel letter8;
    private JLabel letter9;
    private JLabel letter10;
    private JLabel letter11;
    private JLabel letter12;
    private JLabel letter13;
    private JLabel letter14;
    private JLabel letter15;
    private JLabel letter16;
    private JLabel letter17;
    private JLabel letter18;
    private JLabel letter19;
    private JLabel letter20;

    private JLabel[] letters;

    private JList wordsAcceptedList;
    private JList playersList;
    private JPanel panel;
    private JPanel panel_1;
    private JPanel panel_2;
    private JPanel panel_3;
    private JPanel panel_4;
    private JPanel panel_5;
    private JPanel panel_6;
    private JPanel panel_7;
    private JPanel panel_8;
    private JPanel panel_9;
    private JPanel panel_10;
    private JPanel panel_11;
    private JPanel panel_12;
    private JPanel panel_13;
    private JPanel panel_14;
    private JPanel panel_15;
    private JPanel panel_16;
    private JPanel panel_17;
    private JPanel panel_18;
    private JPanel panel_19;
    private JPanel panel_20;
    private JPanel panel_21;
    private JPanel panel_22;
    private JLabel lblTime_1;
    private int round;
    private javax.swing.Timer refreshTimer;
    private javax.swing.Timer countdownTimer;

    private GameServer gameServer;
    public GameRoomFrame(GameServer gameServer, Room room, int round) {
        this.gameServer = gameServer;
        this.room = room;
        this.round = round;
        setTitle("Game Room");
        setResizable(false);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 801, 603);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(255, 128, 255));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

        setContentPane(contentPane);
        contentPane.setLayout(null);

        wordsAcceptedList = new JList();
        wordsAcceptedList.setBounds(10, 50, 120, 503);
        contentPane.add(wordsAcceptedList);

        playersList = new JList();
        playersList.setBounds(644, 50, 131, 503);
        contentPane.add(playersList);

        JLabel lblTime = new JLabel("Time:");
        lblTime.setHorizontalAlignment(SwingConstants.CENTER);
        lblTime.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblTime.setBounds(335, 11, 51, 28);
        contentPane.add(lblTime);

        lblTime_1 = new JLabel("00:00");
        lblTime_1.setHorizontalAlignment(SwingConstants.CENTER);
        lblTime_1.setFont(new Font("Tahoma", Font.BOLD, 16));
        lblTime_1.setBounds(386, 11, 51, 28);
        contentPane.add(lblTime_1);

        userInput = new JTextField();
        userInput.setFont(new Font("Tahoma", Font.PLAIN, 18));
        userInput.setBounds(231, 515, 312, 38);
        contentPane.add(userInput);
        userInput.setColumns(10);

        JLabel letter1 = new JLabel("A");
        letter1.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter1.setHorizontalAlignment(SwingConstants.CENTER);
        letter1.setEnabled(false);
        letter1.setForeground(new Color(0, 0, 0));
        letter1.setBackground(new Color(255, 255, 255));
        letter1.setBounds(177, 143, 96, 45);
        contentPane.add(letter1);

        letter2 = new JLabel("A");
        letter2.setHorizontalAlignment(SwingConstants.CENTER);
        letter2.setForeground(Color.BLACK);
        letter2.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter2.setEnabled(false);
        letter2.setBackground(new Color(255, 255, 128));
        letter2.setBounds(283, 143, 96, 45);
        contentPane.add(letter2);

        letter3 = new JLabel("A");
        letter3.setHorizontalAlignment(SwingConstants.CENTER);
        letter3.setForeground(Color.BLACK);
        letter3.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter3.setEnabled(false);
        letter3.setBackground(new Color(255, 255, 128));
        letter3.setBounds(389, 143, 96, 45);
        contentPane.add(letter3);

        letter4 = new JLabel("A");
        letter4.setHorizontalAlignment(SwingConstants.CENTER);
        letter4.setForeground(Color.BLACK);
        letter4.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter4.setEnabled(false);
        letter4.setBackground(new Color(255, 255, 128));
        letter4.setBounds(495, 143, 96, 45);
        contentPane.add(letter4);

        letter8 = new JLabel("A");
        letter8.setHorizontalAlignment(SwingConstants.CENTER);
        letter8.setForeground(Color.BLACK);
        letter8.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter8.setEnabled(false);
        letter8.setBackground(new Color(255, 255, 128));
        letter8.setBounds(495, 199, 96, 45);
        contentPane.add(letter8);

        letter7 = new JLabel("A");
        letter7.setHorizontalAlignment(SwingConstants.CENTER);
        letter7.setForeground(Color.BLACK);
        letter7.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter7.setEnabled(false);
        letter7.setBackground(new Color(255, 255, 128));
        letter7.setBounds(389, 199, 96, 45);
        contentPane.add(letter7);

        letter6 = new JLabel("A");
        letter6.setHorizontalAlignment(SwingConstants.CENTER);
        letter6.setForeground(Color.BLACK);
        letter6.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter6.setEnabled(false);
        letter6.setBackground(new Color(255, 255, 128));
        letter6.setBounds(283, 199, 96, 45);
        contentPane.add(letter6);

        letter5 = new JLabel("A");
        letter5.setHorizontalAlignment(SwingConstants.CENTER);
        letter5.setForeground(Color.BLACK);
        letter5.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter5.setEnabled(false);
        letter5.setBackground(new Color(255, 255, 128));
        letter5.setBounds(177, 199, 96, 45);
        contentPane.add(letter5);

        letter12 = new JLabel("A");
        letter12.setHorizontalAlignment(SwingConstants.CENTER);
        letter12.setForeground(Color.BLACK);
        letter12.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter12.setEnabled(false);
        letter12.setBackground(new Color(255, 255, 128));
        letter12.setBounds(495, 255, 96, 45);
        contentPane.add(letter12);

        letter11 = new JLabel("A");
        letter11.setHorizontalAlignment(SwingConstants.CENTER);
        letter11.setForeground(Color.BLACK);
        letter11.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter11.setEnabled(false);
        letter11.setBackground(new Color(255, 255, 128));
        letter11.setBounds(389, 255, 96, 45);
        contentPane.add(letter11);

        letter10 = new JLabel("A");
        letter10.setHorizontalAlignment(SwingConstants.CENTER);
        letter10.setForeground(Color.BLACK);
        letter10.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter10.setEnabled(false);
        letter10.setBackground(new Color(255, 255, 128));
        letter10.setBounds(283, 255, 96, 45);
        contentPane.add(letter10);

        letter9 = new JLabel("A");
        letter9.setHorizontalAlignment(SwingConstants.CENTER);
        letter9.setForeground(Color.BLACK);
        letter9.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter9.setEnabled(false);
        letter9.setBackground(new Color(255, 255, 128));
        letter9.setBounds(177, 255, 96, 45);
        contentPane.add(letter9);

        letter16 = new JLabel("A");
        letter16.setHorizontalAlignment(SwingConstants.CENTER);
        letter16.setForeground(Color.BLACK);
        letter16.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter16.setEnabled(false);
        letter16.setBackground(new Color(255, 255, 128));
        letter16.setBounds(495, 311, 96, 45);
        contentPane.add(letter16);

        letter15 = new JLabel("A");
        letter15.setHorizontalAlignment(SwingConstants.CENTER);
        letter15.setForeground(Color.BLACK);
        letter15.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter15.setEnabled(false);
        letter15.setBackground(new Color(255, 255, 128));
        letter15.setBounds(389, 311, 96, 45);
        contentPane.add(letter15);

        letter14 = new JLabel("A");
        letter14.setHorizontalAlignment(SwingConstants.CENTER);
        letter14.setForeground(Color.BLACK);
        letter14.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter14.setEnabled(false);
        letter14.setBackground(new Color(255, 255, 128));
        letter14.setBounds(283, 311, 96, 45);
        contentPane.add(letter14);

        letter13 = new JLabel("A");
        letter13.setHorizontalAlignment(SwingConstants.CENTER);
        letter13.setForeground(Color.BLACK);
        letter13.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter13.setEnabled(false);
        letter13.setBackground(new Color(255, 255, 128));
        letter13.setBounds(177, 311, 96, 45);
        contentPane.add(letter13);

        letter20 = new JLabel("A");
        letter20.setHorizontalAlignment(SwingConstants.CENTER);
        letter20.setForeground(Color.BLACK);
        letter20.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter20.setEnabled(false);
        letter20.setBackground(new Color(255, 255, 128));
        letter20.setBounds(495, 367, 96, 45);
        contentPane.add(letter20);

        letter19 = new JLabel("A");
        letter19.setHorizontalAlignment(SwingConstants.CENTER);
        letter19.setForeground(Color.BLACK);
        letter19.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter19.setEnabled(false);
        letter19.setBackground(new Color(255, 255, 128));
        letter19.setBounds(389, 367, 96, 45);
        contentPane.add(letter19);

        letter18 = new JLabel("A");
        letter18.setHorizontalAlignment(SwingConstants.CENTER);
        letter18.setForeground(Color.BLACK);
        letter18.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter18.setEnabled(false);
        letter18.setBackground(new Color(255, 255, 128));
        letter18.setBounds(283, 367, 96, 45);
        contentPane.add(letter18);

        letter17 = new JLabel("A");
        letter17.setHorizontalAlignment(SwingConstants.CENTER);
        letter17.setForeground(Color.BLACK);
        letter17.setFont(new Font("Tahoma", Font.BOLD, 24));
        letter17.setEnabled(false);
        letter17.setBackground(new Color(255, 255, 128));
        letter17.setBounds(177, 367, 96, 45);
        contentPane.add(letter17);

        panel = new JPanel();
        panel.setBounds(10, 11, 120, 37);
        contentPane.add(panel);

        JLabel lblNewLabel = new JLabel("Words Accepted");
        panel.add(lblNewLabel);
        lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);

        panel_1 = new JPanel();
        panel_1.setBounds(644, 11, 131, 37);
        contentPane.add(panel_1);

        JLabel lblPlayers = new JLabel("Players");
        panel_1.add(lblPlayers);
        lblPlayers.setHorizontalAlignment(SwingConstants.CENTER);
        lblPlayers.setFont(new Font("Tahoma", Font.PLAIN, 16));

        panel_2 = new JPanel();
        panel_2.setBounds(177, 143, 96, 45);
        contentPane.add(panel_2);

        panel_3 = new JPanel();
        panel_3.setBounds(283, 143, 96, 45);
        contentPane.add(panel_3);

        panel_4 = new JPanel();
        panel_4.setBounds(389, 143, 96, 45);
        contentPane.add(panel_4);

        panel_5 = new JPanel();
        panel_5.setBounds(495, 143, 96, 45);
        contentPane.add(panel_5);

        panel_6 = new JPanel();
        panel_6.setBounds(177, 199, 96, 45);
        contentPane.add(panel_6);

        panel_7 = new JPanel();
        panel_7.setBounds(283, 199, 96, 45);
        contentPane.add(panel_7);

        panel_8 = new JPanel();
        panel_8.setBounds(386, 199, 96, 45);
        contentPane.add(panel_8);

        panel_9 = new JPanel();
        panel_9.setBounds(495, 199, 96, 45);
        contentPane.add(panel_9);

        panel_10 = new JPanel();
        panel_10.setBounds(177, 255, 96, 45);
        contentPane.add(panel_10);

        panel_11 = new JPanel();
        panel_11.setBounds(283, 255, 96, 45);
        contentPane.add(panel_11);

        panel_12 = new JPanel();
        panel_12.setBounds(389, 255, 96, 45);
        contentPane.add(panel_12);

        panel_13 = new JPanel();
        panel_13.setBounds(495, 255, 96, 45);
        contentPane.add(panel_13);

        panel_14 = new JPanel();
        panel_14.setBounds(177, 311, 96, 45);
        contentPane.add(panel_14);

        panel_15 = new JPanel();
        panel_15.setBounds(283, 311, 96, 45);
        contentPane.add(panel_15);

        panel_16 = new JPanel();
        panel_16.setBounds(389, 311, 96, 45);
        contentPane.add(panel_16);

        panel_17 = new JPanel();
        panel_17.setBounds(495, 311, 96, 45);
        contentPane.add(panel_17);

        panel_18 = new JPanel();
        panel_18.setBounds(177, 367, 96, 45);
        contentPane.add(panel_18);

        panel_19 = new JPanel();
        panel_19.setBounds(283, 367, 96, 45);
        contentPane.add(panel_19);

        panel_20 = new JPanel();
        panel_20.setBounds(389, 367, 96, 45);
        contentPane.add(panel_20);

        panel_21 = new JPanel();
        panel_21.setBounds(495, 367, 96, 45);
        contentPane.add(panel_21);

        panel_22 = new JPanel();
        panel_22.setBounds(311, 11, 171, 38);
        contentPane.add(panel_22);

        letters = new JLabel[]{
                letter1,
                letter2,
                letter3,
                letter4,
                letter5,
                letter6,
                letter7,
                letter8,
                letter9,
                letter10,
                letter11,
                letter12,
                letter13,
                letter14,
                letter15,
                letter16,
                letter17,
                letter18,
                letter19,
                letter20
        };

        addDummyData();
        initListener();
    }

    private void initListener(){
        userInput.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    String word = userInput.getText().trim();

                    boolean allLettersFound = true;

                    for (int i = 0; i < word.length(); i++) {
                        boolean letterIsInBoard = false;
                        for (int x = 0; x < letters.length; x++) {
                            if (word.charAt(i) == letters[x].getText().charAt(0)) {
                                letterIsInBoard = true;
                                System.out.print(":" + word.charAt(i) + " = " + letters[x].getText());
                                break;
                            }
                        }
                        if (!letterIsInBoard) {
                            allLettersFound = false;
                            break;
                        }
                    }

                    if (!allLettersFound) {
                        JOptionPane.showMessageDialog(
                                null,
                                "Some letters are not found in the board",
                                "Result",
                                JOptionPane.INFORMATION_MESSAGE
                        );
                        return;
                    }



                    if(words.contains(word)){
                        JOptionPane.showMessageDialog(
                                null,
                                "Word is already submitted!",
                                "Result",
                                JOptionPane.INFORMATION_MESSAGE
                        );
                        return;
                    }

                    boolean result = gameServer.submitWord(word, room, UserSession.getInstance().getCurrentUser(), round);

                    String message = result ? "Word submitted successfully." : "Invalid Word.";
                    if(result){

                        words.addElement(word);
                    }
                    wordsAcceptedList.setModel(words);
                    JOptionPane.showMessageDialog(
                            null,
                            message,
                            "Result",
                            JOptionPane.INFORMATION_MESSAGE
                    );
                    userInput.setText("");
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        startCountdownTimer(room, round);
    }




    private void startCountdownTimer(Room room, int rounds) {
        final Room room1 = room;


        gameServer.createTimerGame(room1, UserSession.getInstance().getCurrentUser().userId);



        countdownTimer = new javax.swing.Timer(1000, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {


                int remainingTime = gameServer.waitingTimeGame(room1.roomId);

                if (remainingTime > 0) {
                    lblTime_1.setText(remainingTime + " s");
                    gameServer.updateTimeGame(room1.roomId, remainingTime-1, UserSession.getInstance().getCurrentUser().userId);
                } else {
                    ((javax.swing.Timer) e.getSource()).stop();


                    gameServer.updateRoomStatus(room);
                    gameServer.removeTimerGame(room1.roomId);
                    new ScoreBoardFrame(gameServer, room, rounds).show();
                    hide();
                }
            }
        });
        countdownTimer.start();
    }

    @Override
    public void dispose() {
        if (refreshTimer != null) {
            refreshTimer.stop();
        }
        if (countdownTimer != null) {
            countdownTimer.stop();
        }
        super.dispose();
    }


    private void addDummyData() {
        generateBoard();
        generateLists();
    }

    private void generateBoard() {
        String letter = gameServer.createCharacter(room.roomId, round);
        for (int i = 0; i < letters.length; i++) {
            letters[i].setText(String.valueOf(letter.charAt(i)));
        }
    }

    private void generateLists() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                updatePlayerList();
            }
        }, 0, 200);
    }

    private void updatePlayerList() {
        UsersScore[] usersScores = gameServer.getUserListScore(room, round);


        SwingUtilities.invokeLater(() -> {
            players.clear();
            for (UsersScore usersScore : usersScores) {
                players.addElement(usersScore.username + ":        " + usersScore.score +"pts");
            }
            playersList.setModel(players);
        });
    }
}