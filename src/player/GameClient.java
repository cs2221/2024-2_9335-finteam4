package player;

import BoggleGame.Credentials;
import BoggleGame.GameServer;
import BoggleGame.GameServerHelper;

import org.omg.CORBA.*;
import org.omg.CosNaming.*;

public class GameClient {
    public static void main(String[] args) {

        clientStart();
    }

    private static void clientStart(){
        try {
            String[] orbArgs = {
                    "-ORBInitialHost", "localhost",
                    "-ORBInitialPort", "2809"
            };
            ORB orb = ORB.init(orbArgs, null);

            org.omg.CORBA.Object nameServiceObj = orb.resolve_initial_references("NameService");
            NamingContextExt namingContext = NamingContextExtHelper.narrow(nameServiceObj);

            String serverName = "BoggledGameServer";
            GameServer gameServer = GameServerHelper.narrow(namingContext.resolve_str(serverName));

            if (gameServer == null) {
                System.out.println("Could not find the server: " + serverName);
                return;
            }
            new LoginFrame(gameServer).setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
