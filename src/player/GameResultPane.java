package player;

import BoggleGame.UserResult;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Dimension;

public class GameResultPane extends JPanel {

    private static final long serialVersionUID = 1L;

    public GameResultPane(UserResult userResult) {
        setPreferredSize(new Dimension(750, 54));

        setBounds(10, 76, 750, 54);
        setLayout(null);
        JLabel player4NamePosition = new JLabel(userResult.username);
        player4NamePosition.setBounds(10, 10, 211, 25);
        add(player4NamePosition);
        player4NamePosition.setFont(new Font("Tahoma", Font.PLAIN, 20));

        JLabel player4RoundsWon = new JLabel("Rounds Won: " + userResult.total_win);
        player4RoundsWon.setBounds(305, 10, 138, 25);
        add(player4RoundsWon);
        player4RoundsWon.setFont(new Font("Tahoma", Font.PLAIN, 20));

        JLabel player4WordPoints = new JLabel("Total Word Points: " + userResult.score);
        player4WordPoints.setBounds(480, 10, 204, 25);
        add(player4WordPoints);
        player4WordPoints.setFont(new Font("Tahoma", Font.PLAIN, 20));
    }
}
