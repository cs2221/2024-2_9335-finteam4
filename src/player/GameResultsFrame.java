package player;

import BoggleGame.GameServer;
import BoggleGame.Room;
import BoggleGame.UserResult;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.FlowLayout;

public class GameResultsFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;

    public GameResultsFrame(GameServer gameServer, Room room) {
        setTitle("Game Results");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 802, 609);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(255, 128, 255));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblNewLabel = new JLabel("Winners");
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 24));
        lblNewLabel.setBounds(322, 11, 125, 36);
        contentPane.add(lblNewLabel);

        JButton btnProceed = new JButton("Proceed");
        btnProceed.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnProceed.setBounds(257, 498, 251, 42);
        btnProceed.addActionListener(e ->{
            new MenuFrame(gameServer).show();

            hide();
        });
        contentPane.add(btnProceed);

        JPanel panel = new JPanel();
        panel.setBackground(new Color(255, 0, 255));
        panel.setBounds(10, 76, 766, 404);
        contentPane.add(panel);
        panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        UserResult[] userResults = gameServer.getUserResult(room);

        for (int x = 0; x < userResults.length; x++){
            if(x == 0){
                gameServer.updateWinner(userResults[x].user_id);
            }

            panel.add(new GameResultPane(userResults[x]));
        }
    }
}