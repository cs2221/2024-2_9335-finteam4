package player;

import BoggleGame.GameServer;
import BoggleGame.Room;
import BoggleGame.UsersScore;
import BoggleGame.Word;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

public class ScoreBoardFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private int round;

    private List<JLabel> playerLabels = new <JLabel>ArrayList();
    private List<JList> playerLists = new <JList>ArrayList();
    private List<JLabel> playerPointsLabels = new <JLabel>ArrayList();

    private GameServer gameServer;
    private Room room;
    public ScoreBoardFrame(GameServer gameServer, Room room, int round) {
        this.gameServer = gameServer;

        this.round = round;
        this.room = room;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 801, 604);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(255, 128, 255));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        this.round = round;
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblNewLabel = new JLabel("Round " + round);
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
        lblNewLabel.setBounds(318, 11, 126, 25);
        contentPane.add(lblNewLabel);


        JLabel player1 = new JLabel("");
        player1.setHorizontalAlignment(SwingConstants.CENTER);
        player1.setFont(new Font("Tahoma", Font.PLAIN, 16));
        player1.setBounds(33, 46, 173, 20);
        contentPane.add(player1);
        playerLabels.add(player1);

        JList player1List = new JList();
        player1List.setBounds(33, 77, 173, 389);
        contentPane.add(player1List);

        JList player2List = new JList();
        player2List.setBounds(216, 77, 173, 389);
        contentPane.add(player2List);

        JList player3List = new JList();
        player3List.setBounds(399, 77, 173, 389);
        contentPane.add(player3List);

        JList player4List = new JList();
        player4List.setBounds(582, 77, 173, 389);
        contentPane.add(player4List);

        playerLists.add(player1List);
        playerLists.add(player2List);
        playerLists.add(player3List);
        playerLists.add(player4List);

        JLabel player2 = new JLabel("");
        player2.setHorizontalAlignment(SwingConstants.CENTER);
        player2.setFont(new Font("Tahoma", Font.PLAIN, 16));
        player2.setBounds(216, 47, 173, 20);
        contentPane.add(player2);

        JLabel player3 = new JLabel("");
        player3.setHorizontalAlignment(SwingConstants.CENTER);
        player3.setFont(new Font("Tahoma", Font.PLAIN, 16));
        player3.setBounds(399, 47, 173, 20);
        contentPane.add(player3);


        JLabel player4 = new JLabel("");
        player4.setHorizontalAlignment(SwingConstants.CENTER);
        player4.setFont(new Font("Tahoma", Font.PLAIN, 16));
        player4.setBounds(582, 46, 173, 20);
        contentPane.add(player4);
        playerLabels.add(player2);
        playerLabels.add(player3);
        playerLabels.add(player4);


        JLabel player1Points = new JLabel("");
        player1Points.setHorizontalAlignment(SwingConstants.CENTER);
        player1Points.setFont(new Font("Tahoma", Font.PLAIN, 16));
        player1Points.setBounds(33, 477, 173, 20);
        contentPane.add(player1Points);

        JLabel player2Points = new JLabel("");
        player2Points.setHorizontalAlignment(SwingConstants.CENTER);
        player2Points.setFont(new Font("Tahoma", Font.PLAIN, 16));
        player2Points.setBounds(216, 477, 173, 20);
        contentPane.add(player2Points);

        JLabel player3Points = new JLabel("");
        player3Points.setHorizontalAlignment(SwingConstants.CENTER);
        player3Points.setFont(new Font("Tahoma", Font.PLAIN, 16));
        player3Points.setBounds(399, 477, 173, 20);
        contentPane.add(player3Points);

        JLabel player4Points = new JLabel("");
        player4Points.setHorizontalAlignment(SwingConstants.CENTER);
        player4Points.setFont(new Font("Tahoma", Font.PLAIN, 16));
        player4Points.setBounds(582, 477, 173, 20);
        contentPane.add(player4Points);

        playerPointsLabels.add(player1Points);
        playerPointsLabels.add(player2Points);
        playerPointsLabels.add(player3Points);
        playerPointsLabels.add(player4Points);

        JButton btnContinue = new JButton("Continue");
        btnContinue.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnContinue.setBounds(265, 515, 258, 39);
        btnContinue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


            }
        });

        JPanel panel = new JPanel();
        panel.setBounds(33, 37, 173, 39);
        contentPane.add(panel);

        JPanel panel_1 = new JPanel();
        panel_1.setBounds(216, 37, 173, 39);
        contentPane.add(panel_1);

        JPanel panel_1_1 = new JPanel();
        panel_1_1.setBounds(399, 37, 173, 39);
        contentPane.add(panel_1_1);

        JPanel panel_1_1_1 = new JPanel();
        panel_1_1_1.setBounds(582, 37, 173, 39);
        contentPane.add(panel_1_1_1);

        JPanel panel_1_1_1_1 = new JPanel();
        panel_1_1_1_1.setBounds(33, 469, 173, 39);
        contentPane.add(panel_1_1_1_1);

        JPanel panel_1_1_1_1_1 = new JPanel();
        panel_1_1_1_1_1.setBounds(216, 469, 173, 39);
        contentPane.add(panel_1_1_1_1_1);

        JPanel panel_1_1_1_1_1_1 = new JPanel();
        panel_1_1_1_1_1_1.setBounds(399, 469, 173, 39);
        contentPane.add(panel_1_1_1_1_1_1);

        JPanel panel_1_1_1_1_1_1_1 = new JPanel();
        panel_1_1_1_1_1_1_1.setBounds(582, 469, 173, 39);
        contentPane.add(panel_1_1_1_1_1_1_1);

        UsersScore[] usersScores = gameServer.getUserListScore(room, round);
        updatePlayerScores(usersScores);

        int wait = gameServer.getTime(0) * 1000;

        Timer timer = new Timer(wait, (ActionEvent e) -> {
            if (round == 3) {
                new GameResultsFrame(gameServer, room).show();
                hide();
                return;
            }
            int newround = round + 1;
            new GameRoomFrame(gameServer, room, newround).show();
            hide();
        });
        timer.setRepeats(false);
        timer.start();
    }

    private void updatePlayerScores(UsersScore[] usersScores) {
        for (int i = 0; i < usersScores.length; i++) {
            UsersScore userScore = usersScores[i];
            String username = userScore.username;
            int score = userScore.score;

            playerLabels.get(i).setText(username);

            Word[] words = gameServer.getWord(room, userScore.user_id, round);

            DefaultListModel<String> listModel = new DefaultListModel<>();
            listModel.clear();


            for (Word word : words) {
                listModel.addElement(word.word);
            }

            gameServer.createResult(userScore.user_id, room, round, determineWinner(usersScores, userScore), score);

            playerLists.get(i).setModel(listModel);

            playerPointsLabels.get(i).setText(score + " Points");
        }
    }
    public boolean determineWinner(UsersScore[] usersScores, UsersScore currentScore) {
        int maxScore = Integer.MIN_VALUE;

        for (UsersScore userScore : usersScores) {
            if (userScore.user_id != currentScore.user_id && userScore.score > maxScore) {
                maxScore = userScore.score;
            }
        }

        return currentScore.score > maxScore;
    }
}