package player;

import BoggleGame.Users;

public class UserSession {

    private static UserSession instance;

    private Users currentUser;

    private UserSession() {
        currentUser = null;
    }

    public static synchronized UserSession getInstance() {
        if (instance == null) {
            instance = new UserSession();
        }
        return instance;
    }

    public void setCurrentUser(Users user) {
        this.currentUser = user;
    }

    public Users getCurrentUser() {
        return currentUser;
    }

    public void clearCurrentUser() {
        this.currentUser = null;
    }
}
