package player;

import BoggleGame.GameServer;
import BoggleGame.Users;
import utils.Messenger;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import javax.swing.JPasswordField;

public class LoginFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField tfUsername;
    private JPasswordField passwordField;


    public LoginFrame(GameServer gameServer) {
        setResizable(false);
        setTitle("Boggled Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 640, 524);
        setLocationRelativeTo(null);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(255, 128, 255));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));

        JLabel lblNewLabel = new JLabel("WELCOME TO BOGGLED");
        lblNewLabel.setBackground(new Color(128, 255, 255));
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setVerticalAlignment(SwingConstants.CENTER);
        lblNewLabel.setFont(new Font("Arial", Font.BOLD, 30));
        contentPane.add(lblNewLabel, BorderLayout.NORTH);

        JPanel panel = new JPanel();
        panel.setBackground(new Color(255, 128, 255));
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(null);

        tfUsername = new JTextField();
        tfUsername.setBorder(new LineBorder(new Color(171, 173, 179), 2, true));
        tfUsername.setBounds(130, 99, 354, 40);
        panel.add(tfUsername);
        tfUsername.setColumns(10);
        tfUsername.setFont(new Font("Tahoma", Font.PLAIN, 16));

        JLabel lblNewLabel_1 = new JLabel("Username");
        lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblNewLabel_1.setBounds(130, 64, 168, 29);
        panel.add(lblNewLabel_1);


        JLabel lblNewLabel_1_1 = new JLabel("Password");
        lblNewLabel_1_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblNewLabel_1_1.setBounds(130, 150, 168, 29);
        panel.add(lblNewLabel_1_1);

        JButton btnLogin = new JButton("Login");
        btnLogin.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnLogin.setBounds(130, 276, 354, 40);
        panel.add(btnLogin);

        JButton btnRegister = new JButton("Sign Up");
        btnRegister.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnRegister.setBounds(130, 326, 354, 40);
        panel.add(btnRegister);

        passwordField = new JPasswordField();
        passwordField.setBounds(130, 190, 354, 40);
        passwordField.setFont(new Font("Tahoma", Font.PLAIN, 16));
        panel.add(passwordField);

        JLabel lblNewLabel_2 = new JLabel("");
        lblNewLabel_2.setForeground(Color.RED);
        lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblNewLabel_2.setBounds(130, 379, 354, 40);
        panel.add(lblNewLabel_2);

        btnLogin.addActionListener(e -> {

            try{




                String username = tfUsername.getText().toString();
                String password = new String(passwordField.getPassword());

                if(username.isEmpty() || password.isEmpty()){
                    Messenger.displayError("Enter the username and password", lblNewLabel_2);
                    return;
                }
                Users users = gameServer.loginUser(username);

                if(!users.password.equals(password)){
                    Messenger.displayError("Incorrect Password", lblNewLabel_2);
                    return;
                }

                if(gameServer.isSessionActive(users.userId)){

                    Messenger.displayError("User is already login", lblNewLabel_2);
                    return;
                }

                if(gameServer.startSession(users.userId)){
                    new MenuFrame(gameServer).setVisible(true);
                    UserSession.getInstance().setCurrentUser(users);
                    this.setVisible(false);
                }


            }catch (Exception ex){
                Messenger.displayError("Username not found", lblNewLabel_2);
            }
        });
        btnRegister.addActionListener(e -> {
            new RegisterFrame(gameServer).setVisible(true);
        });
    }
}