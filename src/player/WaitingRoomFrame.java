package player;

import BoggleGame.GameServer;
import BoggleGame.Room;
import BoggleGame.Users;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WaitingRoomFrame extends JFrame {

    private JPanel contentPane;
    private JTable table;
    private DefaultTableModel model;
    private Timer refreshTimer;
    private Timer countdownTimer;
    private JLabel lblNewLabel;
    private GameServer gameServer;
    private Room room;

    public WaitingRoomFrame(GameServer gameServer, Room room) {
        setBackground(new Color(255, 128, 255));
        this.gameServer = gameServer;
        this.room = room;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 811, 630);
        setLocationRelativeTo(null);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(255, 128, 255));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));

        JLabel lblTitle = new JLabel("Waiting Room");
        lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
        lblTitle.setPreferredSize(new Dimension(43, 50));
        contentPane.add(lblTitle, BorderLayout.NORTH);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(new BorderLayout(0, 0));

        model = new DefaultTableModel(new String[] { "Player" }, 0);
        table = new JTable(model);

        table.getTableHeader().setVisible(false);
        table.getTableHeader().setPreferredSize(new Dimension(0, 0));
        table.setRowHeight(40);
        table.setIntercellSpacing(new Dimension(10, 10));

        DefaultTableCellRenderer customRenderer = new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                cell.setFont(new Font("Tahoma", Font.PLAIN, 16));
                cell.setBackground(new Color(173, 216, 230));
                return cell;
            }
        };

        for (int i = 0; i < table.getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setCellRenderer(customRenderer);
        }

        JScrollPane scrollPane = new JScrollPane(table);
        panel.add(scrollPane, BorderLayout.CENTER);

        JPanel bottomPanel = new JPanel();
        bottomPanel.setPreferredSize(new Dimension(10, 100));
        bottomPanel.setLayout(null);

//        JButton btnWaitingForOther = new JButton("Waiting for other players to join...");
//        btnWaitingForOther.setFont(new Font("Tahoma", Font.BOLD, 20));
//        btnWaitingForOther.setBounds(194, 11, 396, 42);
//        bottomPanel.add(btnWaitingForOther);

        contentPane.add(bottomPanel, BorderLayout.SOUTH);

        lblNewLabel = new JLabel("");
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblNewLabel.setBounds(321, 60, 144, 29);
        bottomPanel.add(lblNewLabel);

        startCountdownTimer(room);

        startRefreshTimer(gameServer, room);
    }

    private void startRefreshTimer(GameServer gameServer, Room room) {
        refreshTimer = new Timer(200, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refreshUserList(gameServer, room);
            }
        });
        refreshTimer.start();
    }

    private void refreshUserList(GameServer gameServer, Room room) {
        Users[] users = gameServer.getUserList(room);


        model.setRowCount(0);

        for (Users user : users) {
            model.addRow(new Object[] { user.username });
        }
    }

    private void startCountdownTimer(Room room) {
        final Room room1 = room;

        if(!gameServer.isTimerExist(room1.roomId, UserSession.getInstance().getCurrentUser().userId)){


            gameServer.createTimer(room1, UserSession.getInstance().getCurrentUser().userId);
        }
        countdownTimer = new Timer(1000, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {


                int remainingTime = gameServer.waitingTime(room1.roomId);
                System.out.println(gameServer.waitingTime(room1.roomId) + " :" + room1.roomId);
                if (remainingTime > 0) {
                    lblNewLabel.setText(remainingTime + " s");
                    gameServer.updateTime(room1.roomId, remainingTime-1, UserSession.getInstance().getCurrentUser().userId);
                } else {
                    ((Timer) e.getSource()).stop();

                    Users[] users = gameServer.getUserList(room);
                    if (users.length == 0) {
                        JOptionPane.showMessageDialog(
                                null,
                                "Match failed. No players joined.",
                                "Match Failed",
                                JOptionPane.ERROR_MESSAGE
                        );
                        gameServer.removeTimerGame(room.roomId);
                        new MenuFrame(gameServer).show();
                    } else if (users.length == 1) {
                        JOptionPane.showMessageDialog(
                                null,
                                "Match failed. Only one player joined.",
                                "Match Failed",
                                JOptionPane.ERROR_MESSAGE
                        );
                        gameServer.updateRoomStatus(room);
                        gameServer.removeTime(room.roomId);
                        new MenuFrame(gameServer).show();
                        hide();
                    } else {
                        JOptionPane.showMessageDialog(
                                null,
                                "Time's up! Match will start.",
                                "Match Start",
                                JOptionPane.INFORMATION_MESSAGE
                        );
                        gameServer.updateRoomStatus(room);
                        gameServer.removeTimerGame(room.roomId);
                        new GameRoomFrame(gameServer, room, 1).show();
                        hide();
                    }


                }
            }
        });
        countdownTimer.start();
    }

    @Override
    public void dispose() {
        if (refreshTimer != null) {
            refreshTimer.stop();
        }
        if (countdownTimer != null) {
            countdownTimer.stop();
        }
        super.dispose();
    }
}