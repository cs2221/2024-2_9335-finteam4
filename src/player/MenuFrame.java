package player;

import BoggleGame.GameServer;
import BoggleGame.Room;
import BoggleGame.Users;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;

public class MenuFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;

    public MenuFrame(GameServer gameServer) {
        setTitle("Game Lobby");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(92, -37, 809, 605);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(255, 128, 255));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

        setContentPane(contentPane);
        setLocationRelativeTo(null);
        contentPane.setLayout(null);

        JLabel lblNewLabel = new JLabel("WELCOME TO BOGGLED");
        lblNewLabel.setVerticalAlignment(SwingConstants.CENTER);
        lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lblNewLabel.setFont(new Font("Arial", Font.BOLD, 30));
        lblNewLabel.setBackground(Color.CYAN);
        lblNewLabel.setBounds(70, 40, 632, 36);
        contentPane.add(lblNewLabel);

        JButton btnNewButton = new JButton("START");
        btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnNewButton.setBounds(191, 208, 395, 48);
        btnNewButton.addActionListener(e->{
            Room room = gameServer.findActiveRoom(UserSession.getInstance().getCurrentUser());
            new WaitingRoomFrame(gameServer, room).setVisible(true);
            hide();
        });

        contentPane.add(btnNewButton);


        JButton btnNewButton_1 = new JButton("LEADERBOARDS");
        btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnNewButton_1.setBounds(191, 267, 395, 48);
        contentPane.add(btnNewButton_1);

        btnNewButton_1.addActionListener(e ->{
            new LeaderboardFrame(gameServer).show();

        });

        JButton btnNewButton_1_1 = new JButton("LOG OUT");
        btnNewButton_1_1.setFont(new Font("Tahoma", Font.BOLD, 20));
        btnNewButton_1_1.setBounds(191, 326, 395, 48);
        contentPane.add(btnNewButton_1_1);

        btnNewButton_1_1.addActionListener(e -> {
            Users user = UserSession.getInstance().getCurrentUser();
            if(gameServer.endSession(user.userId)){
                UserSession.getInstance().clearCurrentUser();
                new LoginFrame(gameServer).show();
                hide();
            }

        });
    }

}