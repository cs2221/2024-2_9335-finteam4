-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2024 at 05:07 PM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bogglegamedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `character`
--

CREATE TABLE `character` (
  `character_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `round` int(11) NOT NULL,
  `letter` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `character`
--

INSERT INTO `character` (`character_id`, `room_id`, `round`, `letter`) VALUES
(1, 1, 1, 'obcykcswxerkbaguajie'),
(2, 7, 1, 'uiibgovmilrfyokacrwc'),
(3, 8, 1, 'ukgcaoucmmuzsywiuwtf'),
(4, 9, 1, 'agvanvaoesvaedqwzxzc'),
(5, 10, 1, 'epupogomuffoazcndbrr'),
(6, 11, 1, 'aauiqivuuzsnzlqbhfrg'),
(7, 11, 2, 'eugipvsiuuiblcvzlqbz'),
(8, 11, 3, 'iiaqieimejbrllspxpym'),
(9, 12, 1, 'ezvadoaewtgeomggnzbd'),
(10, 11, 4, 'peeosleejuapzzxndjkn'),
(11, 12, 2, 'eseaaqknaatxsohydgkt'),
(12, 12, 3, 'fzlofwanigaobcaihxrc'),
(13, 12, 4, 'yspyyfeaueehocbadznh'),
(14, 13, 1, 'vseyaorfiuuudbtwzqmn'),
(15, 13, 2, 'cvunojiqujiiikkzknsd'),
(16, 13, 3, 'arooeivewmfocxpgfspt'),
(17, 14, 1, 'kiauidwewiiffbtzlzjr'),
(18, 15, 1, 'oowaqoohipanmjdwzvms'),
(19, 15, 2, 'enwaeuaoazxsfldlgwtp'),
(20, 15, 3, 'raceeirwoigmethlgyvx'),
(21, 16, 1, 'iusthuuxokkwapfkyrto'),
(22, 17, 1, 'mayeiihdsleiigrjkyss'),
(23, 18, 1, 'rizgurafaerlmzobsrak'),
(24, 18, 2, 'seeiasleuugwlstgyyrx'),
(25, 19, 1, 'iaexoxazuroxpzmxcbjk'),
(26, 19, 2, 'uooipoienlhplmdwtvvl'),
(27, 20, 1, 'lyiopootdmzaacwocfgb'),
(28, 20, 2, 'aivouowleejgnlcmckcs'),
(29, 20, 3, 'uamiyoetbjblfygvgazu'),
(30, 20, 4, 'buoortevvefonoxwxpcb'),
(31, 21, 1, 'adienaogwuodlclgvdrq'),
(32, 21, 2, 'ckfwurtuyoficdvgjeao'),
(33, 21, 3, 'znfiehaavyiepdukgspv'),
(34, 24, 1, 'wiuwhiesucedqybyixds'),
(35, 24, 2, 'kaexaeiudikqtkjfrhwh'),
(36, 24, 3, 'ueunploioojltfwscrry'),
(37, 25, 1, 'aynzdooooguvninjbhdy'),
(38, 25, 2, 'aaueejaoqzyjnylhmghp'),
(39, 25, 3, 'zhdazkyugeojhpeohidx'),
(40, 26, 1, 'reuiexuirjuvtyvkvyyr'),
(41, 26, 2, 'gwotojrwvziekjafddia'),
(42, 26, 3, 'duelltzilioseumkfnmj'),
(43, 27, 1, 'wimriuaafebovytrzvjx'),
(44, 27, 2, 'jfclponxejamabempyoo'),
(45, 27, 3, 'vkeijhjeudzkeiwitpwj'),
(46, 28, 1, 'vaxxwaplqcpjoiumaojz'),
(47, 28, 2, 'elpzciouaaomkrbnrngh'),
(48, 28, 3, 'oeapleczutqzfewoplbn'),
(49, 29, 1, 'kkjoisohesqhoconegxv'),
(50, 29, 2, 'ewluurocyvkjkjouhceg'),
(51, 29, 3, 'uohaueaimmzwbhdglgdk'),
(52, 30, 1, 'glviexedbpyaoscbkake'),
(53, 31, 1, 'qaditauuaiplcsqrrmkw'),
(54, 32, 1, 'mauolmioavunmxnqbybr'),
(55, 33, 1, 'erooaaouzqzzsyzcfyfw'),
(56, 34, 1, 'xmivoiucauaxktgdwsqb'),
(57, 35, 1, 'asasgwwacazfghuiiyhg'),
(58, 36, 1, 'relzmjubpdxazbvseoio'),
(59, 36, 2, 'ffucpoedseirpcqfuuhd'),
(60, 36, 3, 'oinaegaaewmxqqrkyqrt'),
(61, 37, 1, 'vpdnotouuioujgvbwvvy');

-- --------------------------------------------------------

--
-- Table structure for table `lobby`
--

CREATE TABLE `lobby` (
  `lobbyId` int(11) NOT NULL,
  `roomId` int(11) NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `lobby`
--

INSERT INTO `lobby` (`lobbyId`, `roomId`, `userId`) VALUES
(6, 1, 5),
(7, 1, 1),
(9, 1, 2),
(10, 2, 1),
(11, 3, 1),
(12, 4, 1),
(13, 5, 1),
(14, 6, 1),
(15, 7, 1),
(16, 8, 1),
(17, 9, 1),
(18, 10, 1),
(19, 11, 1),
(20, 12, 1),
(21, 13, 1),
(22, 14, 1),
(23, 15, 1),
(24, 16, 1),
(25, 17, 1),
(26, 18, 1),
(27, 19, 1),
(28, 20, 1),
(29, 21, 1),
(30, 21, 2),
(31, 22, 1),
(32, 23, 1),
(33, 24, 1),
(34, 25, 1),
(35, 26, 1),
(36, 27, 2),
(37, 28, 1),
(38, 29, 1),
(39, 30, 1),
(40, 31, 1),
(41, 32, 1),
(42, 33, 1),
(43, 34, 1),
(44, 35, 1),
(45, 36, 1),
(46, 37, 1);

-- --------------------------------------------------------

--
-- Table structure for table `playing`
--

CREATE TABLE `playing` (
  `playing_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `round` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `room` int(11) NOT NULL,
  `round` int(11) NOT NULL,
  `isWinner` tinyint(1) NOT NULL,
  `point` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `user_id`, `room`, `round`, `isWinner`, `point`) VALUES
(1, 1, 25, 1, 1, 5),
(2, 1, 25, 3, 1, 5),
(3, 1, 26, 1, 1, 13),
(4, 1, 26, 2, 1, 8),
(5, 1, 26, 3, 1, 8),
(6, 2, 27, 1, 1, 13),
(7, 2, 27, 2, 1, 8),
(8, 2, 27, 3, 1, 13),
(9, 1, 28, 1, 1, 5),
(10, 1, 28, 2, 1, 5),
(11, 1, 28, 3, 1, 5),
(12, 1, 29, 2, 1, 5),
(13, 1, 29, 3, 1, 5),
(14, 1, 36, 1, 1, 21),
(15, 1, 36, 3, 1, 35);

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `roomId` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`roomId`, `status`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(11, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0),
(21, 0),
(22, 0),
(23, 0),
(24, 0),
(25, 0),
(26, 0),
(27, 0),
(28, 0),
(29, 0),
(30, 0),
(31, 0),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0);

-- --------------------------------------------------------

--
-- Table structure for table `timer`
--

CREATE TABLE `timer` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `second` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `timer`
--

INSERT INTO `timer` (`id`, `type`, `second`) VALUES
(1, 1, 100),
(2, 0, 30);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `win` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `username`, `password`, `win`) VALUES
(3, 'tomie', '123', 0),
(4, 'user', '123', 0),
(5, 'ramil', '123', 0),
(36, 'Jovv', '123', 0),
(37, 'Ian', '123', 0),
(38, 'Trevor', '123', 0),
(39, 'username', 'password', 0);

-- --------------------------------------------------------

--
-- Table structure for table `word_user`
--

CREATE TABLE `word_user` (
  `word_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `round` int(11) NOT NULL,
  `word` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `word_user`
--

INSERT INTO `word_user` (`word_id`, `user_id`, `room_id`, `round`, `word`) VALUES
(1, 1, 1, 1, 'dasdas'),
(2, 2, 1, 1, 'dasdasa'),
(3, 1, 1, 1, 'abases'),
(4, 1, 1, 1, 'abases'),
(5, 1, 1, 1, 'abases'),
(6, 1, 1, 1, 'laden'),
(7, 1, 1, 1, 'laden'),
(8, 1, 1, 1, 'laden'),
(9, 1, 1, 1, 'laden'),
(10, 1, 7, 1, 'laden'),
(11, 1, 11, 2, 'laden'),
(12, 1, 11, 2, 'dads'),
(13, 1, 12, 1, 'laden'),
(14, 1, 15, 2, 'laden'),
(15, 1, 16, 1, 'laden'),
(16, 1, 16, 1, 'laden'),
(17, 1, 17, 1, 'abasing'),
(18, 1, 18, 2, 'abasing'),
(19, 1, 19, 1, 'abasing'),
(20, 1, 20, 1, 'laden'),
(21, 1, 20, 2, 'laden'),
(22, 1, 20, 3, 'laden'),
(23, 1, 21, 1, 'laden'),
(24, 1, 21, 2, 'laden'),
(25, 1, 24, 1, 'abasing'),
(26, 1, 24, 2, 'abasing'),
(27, 1, 24, 2, 'abate'),
(28, 1, 24, 3, 'abate'),
(29, 1, 24, 3, 'abate'),
(30, 1, 24, 3, 'abate'),
(31, 1, 24, 3, 'abating'),
(32, 1, 25, 1, 'laden'),
(33, 1, 25, 3, 'laden'),
(34, 1, 26, 1, 'abdicate'),
(35, 1, 26, 1, 'laden'),
(36, 1, 26, 1, 'laden'),
(37, 1, 26, 2, 'abdicate'),
(38, 1, 26, 2, 'abdicate'),
(39, 1, 26, 2, 'abdicate'),
(40, 1, 26, 3, 'abdicate'),
(41, 1, 26, 3, 'abdicate'),
(42, 2, 27, 1, 'laden'),
(43, 2, 27, 1, 'abdicate'),
(44, 2, 27, 2, 'abdicate'),
(45, 2, 27, 2, 'abdicate'),
(46, 2, 27, 3, 'abdicate'),
(47, 2, 27, 3, 'laden'),
(48, 1, 28, 1, 'laden'),
(49, 1, 28, 2, 'laden'),
(50, 1, 28, 2, 'laden'),
(51, 1, 28, 2, 'laden'),
(52, 1, 28, 2, 'laden'),
(53, 1, 28, 2, 'laden'),
(54, 1, 28, 3, 'laden'),
(55, 1, 28, 3, 'laden'),
(56, 1, 28, 3, 'laden'),
(57, 1, 28, 3, 'laden'),
(58, 1, 28, 3, 'laden'),
(59, 1, 29, 2, 'laden'),
(60, 1, 29, 3, 'laden'),
(61, 1, 29, 3, 'laden'),
(62, 1, 29, 3, 'laden'),
(63, 1, 29, 3, 'laden'),
(64, 1, 36, 1, 'laden'),
(65, 1, 36, 1, 'abnegation'),
(66, 1, 36, 1, 'abodes'),
(67, 1, 36, 3, 'abrading'),
(68, 1, 36, 3, 'acquire'),
(69, 1, 36, 3, 'acquirers'),
(70, 1, 36, 3, 'acquisition');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `character`
--
ALTER TABLE `character`
  ADD PRIMARY KEY (`character_id`);

--
-- Indexes for table `lobby`
--
ALTER TABLE `lobby`
  ADD PRIMARY KEY (`lobbyId`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`roomId`);

--
-- Indexes for table `timer`
--
ALTER TABLE `timer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `word_user`
--
ALTER TABLE `word_user`
  ADD PRIMARY KEY (`word_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `character`
--
ALTER TABLE `character`
  MODIFY `character_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `lobby`
--
ALTER TABLE `lobby`
  MODIFY `lobbyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `roomId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `timer`
--
ALTER TABLE `timer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `word_user`
--
ALTER TABLE `word_user`
  MODIFY `word_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
